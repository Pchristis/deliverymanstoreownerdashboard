import { analysisConstants, orderanalysisConstants } from '../constants';
import { history } from '../helpers';
import { apiConstants } from '../constants';
import { getCookie } from '../utils/cookies';

import axios from 'axios';


export const analysisActions = {
    filterAnalysis,
    orderAnalysis
};

function filterAnalysis(startDateTS, endDateTS, storeId) {
  return dispatch => {
    const config = {
       method:"GET",
       url: `${apiConstants.BASE_URL}/api/filterAnalysis?from=${startDateTS}&to=${endDateTS}&shop_id=${storeId}&lang=el`,
     };

     config.headers = {
        'content-type': 'application/json',
        'Authorization': `Bearer ${getCookie('token')}`
     };

     axios.request(config).then(
      response =>{
          dispatch(success(response.data));
        }
      );
    }

    function success(analysis) { return { type: analysisConstants.ANALYSIS_SUCCESS, analysis } }

}


function orderAnalysis(startDateTS, endDateTS, storeId) {
  return dispatch => {
    const config = {
       method:"GET",
       url: `${apiConstants.BASE_URL}/api/ordersAnalysis?from=${startDateTS}&to=${endDateTS}&shop_id=${storeId}`,
     };

     config.headers = {
        'content-type': 'application/json',
        'Authorization': `Bearer ${getCookie('token')}`
     };

     axios.request(config).then(
      response =>{
          dispatch(success(response.data));
        }
      );
    }

    function success(orderanalysis) { return { type: orderanalysisConstants.ORDER_ANALYSIS_SUCCESS, orderanalysis } }

}
