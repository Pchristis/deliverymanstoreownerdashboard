import { menuConstants } from '../constants';
import { history } from '../helpers';
import { apiConstants } from '../constants';
import { getCookie } from '../utils/cookies';

import axios from 'axios';


export const menuActions = {
    filterMenu,
    updateMenuStatus,
    updateMenuAvailableHours,
    
};

function filterMenu(storeId) {
  return dispatch => {
    const config = {
       method:"GET",
       url: `${apiConstants.BASE_URL}/api/menu?&shop_id=${storeId}&lang=el`,
     };

     config.headers = {
        'content-type': 'application/json',
        'Authorization': `Bearer ${getCookie('token')}`
     };

     axios.request(config).then(
      response =>{
          dispatch(success(response.data.Menu));
        }
      );
    }

    function success(menu) { return { type: menuConstants.MENU_SUCCESS, menu } }

}



function updateMenuAvailableHours(menuAlias, menuItemAlias, menuOptionAlias, menuItemOptionAlias, storeId, availableHours) {
  return dispatch => {
    const config = {
       method:"POST",
       url: `${apiConstants.BASE_URL}/api/setMenuOptionItemState`,
       data: {
         'menu_alias': menuAlias,
         'menu_item_alias': (menuItemAlias?menuItemAlias:''),
         'option_category_alias': (menuOptionAlias?menuOptionAlias:''),
         'option_item_alias': (menuItemOptionAlias?menuItemOptionAlias:''),
         'shop_id': storeId,
         'availableHour': availableHours
        }
     };

     config.headers = {
        'content-type': 'application/json',
        'Authorization': `Bearer ${getCookie('token')}`
     };

     axios.request(config).then(
      response =>{
        return dispatch(filterMenu(storeId));
        }
      );
    }
}

function updateMenuStatus(menuAlias, menuItemAlias, menuOptionAlias, menuItemOptionAlias, storeId, state) {
  return dispatch => {
    const config = {
       method:"POST",
       url: `${apiConstants.BASE_URL}/api/setMenuItemState`,
       data: {
         'menu_alias': menuAlias,
         'menu_item_alias': menuItemAlias,
         'menu_option_alias': menuOptionAlias,
         'menu_item_option_alias': menuItemOptionAlias,
         'state': state,
         'shop_id': storeId
        }
     };

     config.headers = {
        'content-type': 'application/json',
        'Authorization': `Bearer ${getCookie('token')}`
     };

     axios.request(config).then(
      response =>{
        return dispatch(filterMenu(storeId));
        }
      );
    }

}
