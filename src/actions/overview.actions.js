import { overviewConstants } from '../constants';
import { history } from '../helpers';
import { apiConstants } from '../constants';
import { getCookie } from '../utils/cookies';

import axios from 'axios';


export const overviewActions = {
    filterOverview
};

function filterOverview(startDateTS, endDateTS, categoriesIds, storeId) {
  return dispatch => {
    const config = {
       method:"POST",
       url: `${apiConstants.BASE_URL}/api/filterOverview`,
       data: {
          'lang':'en',
          'from': startDateTS,
          'to':endDateTS,
          'selectedCats': categoriesIds,
          'shop': storeId
        }
     };

     config.headers = {
        'content-type': 'application/json',
        'Authorization': `Bearer ${getCookie('token')}`
     };

     axios.request(config).then(
      response =>{
          dispatch(success(response.data));
        }
      );
    }

    function success(overview) { return { type: overviewConstants.OVERVIEW_SUCCESS, overview } }

}
