import { userConstants } from '../constants';
import { userService } from '../services';
import { history } from '../helpers';
import { apiConstants } from '../constants';
import { getCookie } from '../utils/cookies';

import axios from 'axios';


export const userActions = {
    login,
    socialMediaLogin,
    logout,
    checkifuserexists,
    getUserInfo,
    getUserAuth,
    updateUserInfo,
    changeUserPassword
};

function login(username, password) {
    return dispatch => {
        dispatch(request({ username }));
        try {
          dispatch(userService.login(username, password))
              .then(
                  user => {
                      dispatch(success(user));
                      history.push('/');
                  }
              );
        } catch (e) {
        }
    };

    function request(user) { return { type: userConstants.LOGIN_REQUEST, user } }
    function failure(error) { return { type: userConstants.LOGIN_FAILURE, error } }
    function success(user) { return { type: userConstants.LOGIN_SUCCESS, user } }
}

function socialMediaLogin(media) {
    return dispatch => {
        userService.socialMediaLogin(media)
            .then(
                user => {
                    dispatch(success(user));
                    history.push('/');
                },
                error => {
                    dispatch(failure(error));
                }
            );
    };

    function success(user) { return { type: userConstants.LOGIN_SUCCESS, user } }
    function failure(error) { return { type: userConstants.LOGIN_FAILURE, error } }
}

function checkifuserexists(username) {
  return dispatch => {
    const config = {
       method:"POST",
       url: `${apiConstants.BASE_URL}/api/checkifUserExists`,
       data: {
         'email': username,
        }
     };

     axios.request(config).then(
      response =>{
        const config = {
           method:"POST",
           url: `${apiConstants.AUTH_URL}/dbconnections/change_password`,
           data: {
             'client_id': `${apiConstants.CLIENT_ID}`,
             'email':username,
             'connection': "Username-Password-Authentication"
            }
         };

         config.headers = {
            'content-type': 'application/json'
          };

         axios.request(config).then(
          response =>{
              dispatch(request(response));
            }
          );

        }
      );
    }

    function request(user) { return { type: userConstants.FORGOT_REQUEST, user } }

}

function logout() {
    userService.logout();
    return { type: userConstants.LOGOUT };
}


function updateUserInfo(firstname, lastname) {
  return (dispatch) => {
  const config = {
     method:"POST",
     url: `${apiConstants.BASE_URL}/api/setPersonalDetails`,
     data: {
       'name': firstname,
       'surname': lastname,
      }
   };

   config.headers = {
      'content-type': 'application/json',
      'Authorization': `Bearer ${getCookie('token')}`
   };

   axios.request(config).then(response=>{

   });
 }

}

function getUserInfo() {
  return (dispatch) => {
      const config={
        method:"GET",
        url: `${apiConstants.BASE_URL}/api/getUserDetails`
        }

      config.headers = {
           'content-type': 'application/json',
           'Authorization': `Bearer ${getCookie('token')}`
      };

      return axios.request(config)
        .then(
          response => {
            dispatch(success(response.data[0]));
          }
        );
   }

   function success(user) { return { type: userConstants.UPDATE_USER_INFO, user } }

}


function getUserAuth() {
    return { type: userConstants.IS_LOGGED_IN };
}


function changeUserPassword(subId, password) {
  return (dispatch) => {
      const config = {
         method:"POST",
         crossDomain: true,
         url: `${apiConstants.AUTH_URL}/oauth/token`,
         data: {
           'grant_type': 'client_credentials',
           'client_id': apiConstants.CLIENT_ID,
           'client_secret': apiConstants.CLIENT_SECRET,
           'audience': apiConstants.AUTH_URL + apiConstants.AUDIENCE_URL
          }
       };

       config.headers = {
          'content-type': 'application/json'
       };

      axios.request(config)
        .then(
          response => {
            const token = response.data.access_token;
            const config2={
              method:"PATCH",
              url: `${apiConstants.AUTH_URL}${apiConstants.AUDIENCE_URL}users/${subId}`,
              data:{
              	"password": password,
                "connection": "Username-Password-Authentication"
                }
              }

            config2.headers = {
                 'content-type': 'application/json',
                 'Authorization': `Bearer ${token}`
               };

            axios.request(config2)
              .then(
                response2=>{
                  // console.log(response2);
                })
              .catch(error=> {
                // console.log('change pass erro',error,error.response,error.response.data.message);
                });
          }
        )
   }

}
