import { menuCategoriesConstants } from '../constants';
import { history } from '../helpers';
import { apiConstants } from '../constants';
import { getCookie } from '../utils/cookies';
import axios from 'axios';


export const menuCategoriesActions = {
    getStoreCategories
};

function getStoreCategories(storeId) {
    return dispatch => {
      const config = {
         method:"POST",
         url: `${apiConstants.BASE_URL}/api/getMenuCategoriesByStoreId`,
         data: {
          "storeId": storeId,
          "languages":["el","en"]
         }
       };
  
       config.headers = {
          'content-type': 'application/json',
          'Authorization': `Bearer ${getCookie('token')}`
       };
  
       axios.request(config).then(
        response =>{
            dispatch(success(response.data))
            console.log("Response Data:",response.data)
          }
        );
      }
      function success(categories) { return {type:menuCategoriesConstants.CATEGORIES_SUCCESS, categories:categories} } 

  }