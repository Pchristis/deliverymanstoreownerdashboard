import { restaurantcontrolConstants } from '../constants';
import { history } from '../helpers';
import { apiConstants } from '../constants';
import { getCookie } from '../utils/cookies';

import axios from 'axios';


export const restaurantcontrolActions = {
    getRestaurantControl,
    updateRestaurantZones,
    updateRestaurantControl
};

function getRestaurantControl() {
  return dispatch => {
    const config = {
       method:"GET",
       url: `${apiConstants.BASE_URL}/api/restaurantControl`,
     };

     config.headers = {
        'content-type': 'application/json',
        'Authorization': `Bearer ${getCookie('token')}`
     };

     axios.request(config).then(
      response =>{
          dispatch(success(response.data.RestaurantControl));
        }
      );
    }

    function success(restaurantcontrol) { return { type: restaurantcontrolConstants.RC_GET_DATA_SUCCESS, restaurantcontrol } }

}


function updateRestaurantControl(restaurantsData) {

  for (var i = 0; i < restaurantsData.length; i++){
    delete restaurantsData[i]['restaurant_description'];
  }

  return dispatch => {
    const config = {
       method:"POST",
       url: `${apiConstants.BASE_URL}/api/setRestaurantControl`,
       data: {
          'restaurants_data': JSON.stringify(restaurantsData)
        }
     };

     config.headers = {
        'content-type': 'application/json',
        'Authorization': `Bearer ${getCookie('token')}`
     };

     axios.request(config).then(
      response =>{
        return dispatch(getRestaurantControl());
        }
      );
    }

}



function updateRestaurantZones(restaurantZoneData) {

  return dispatch => {
    const config = {
       method:"POST",
       url: `${apiConstants.BASE_URL}/api/changeRestaurantZoneStatus`,
       data: restaurantZoneData
     };

     config.headers = {
        'content-type': 'application/json',
        'Authorization': `Bearer ${getCookie('token')}`
     };

     axios.request(config).then(
      response =>{
        document.querySelector('.p-overlaypanel-close').click();
        return dispatch(getRestaurantControl());
        }
      );
    }

}
