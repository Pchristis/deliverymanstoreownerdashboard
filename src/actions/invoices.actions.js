import { invoicesConstants } from '../constants';
import { history } from '../helpers';
import { apiConstants } from '../constants';
import { getCookie } from '../utils/cookies';

import axios from 'axios';


export const invoicesActions = {
    loginToApiCall,
    filterInvoices,
    shareLinkOfInvoice,
    getInvoicePDF,
    downloadZip
};

function filterInvoices(startDate, endDate, storesIds) {
  return dispatch => {
    let storesParameter = '';
    if (storesIds){
      for (var i = 0; i < storesIds.length; i++) {
        storesParameter += '&search[customerids][]=' + storesIds[i];
      }
    }

    const config = {
       method:"GET",
       url: `${apiConstants.DM_API_URL}/filter-invoices/search%5Bdate_min%5D=${startDate}&search%5Bdate_max%5D=${endDate}&sort=invoice_date_desc${storesParameter}`,
     };

     config.headers = {
        'content-type': 'application/json',
        'Authorization': `Bearer ${localStorage.getItem('dm-api-token')}`
     };

     axios.request(config).then(
      response =>{
          dispatch(success(response.data.response.result.invoices));
        }
      );
    }

    function success(invoices) { return { type: invoicesConstants.INVOICES_SUCCESS, invoices } }

}

function shareLinkOfInvoice(invoiceId) {
  const config = {
     method:"GET",
     url: `${apiConstants.DM_API_URL}/share-link-invoice/${invoiceId}`,
   };
   config.headers = {
      'content-type': 'application/json',
      'Authorization': `Bearer ${localStorage.getItem('dm-api-token')}`
   };
   axios.request(config).then(
    response =>{
        const link = document.createElement('a');
        link.href = response.data.response.result.share_link.share_link;
        link.target = "_blank";
        document.body.appendChild(link);
        link.click();
      }
    );
}


function downloadZip(invoicesJson) {
  window.$('#zip-loader>span:first-child').removeClass('pi-download');
  window.$('#zip-loader>span:first-child').addClass('pi-spin pi-spinner');
  const config = {
    method:"GET",
    url: `${apiConstants.DM_API_URL}/invoice-zip/${invoicesJson}`,
  };
  config.headers = {
     'content-type': 'application/json',
     'Authorization': `Bearer ${localStorage.getItem('dm-api-token')}`
  };
  axios.request(config).then(
     response =>{
       window.$('#zip-loader>span:first-child').addClass('pi-download');
       window.$('#zip-loader>span:first-child').removeClass('pi-spin pi-spinner');
       const link = document.createElement('a');
       link.href = response['data'];
       link.setAttribute('download', ''); //or any other extension
       document.body.appendChild(link);
       link.click();
     }
  );
}

function getInvoicePDF(invoiceId, filename) {
  window.$('#'+invoiceId+'-loader>span:first-child').removeClass('pi-download');
  window.$('#'+invoiceId+'-loader>span:first-child').addClass('pi-spin pi-spinner');
  const config = {
     method:"GET",
     url: `${apiConstants.DM_API_URL}/download-invoice/${invoiceId}/${filename}`,
   };
   config.headers = {
      'content-type': 'application/json',
      'Authorization': `Bearer ${localStorage.getItem('dm-api-token')}`
   };
   axios.request(config).then(
    response =>{
      window.$('#'+invoiceId+'-loader>span:first-child').addClass('pi-download');
      window.$('#'+invoiceId+'-loader>span:first-child').removeClass('pi-spin pi-spinner');
       const link = document.createElement('a');
       link.href = response['data'];
       link.setAttribute('download', filename); //or any other extension
       document.body.appendChild(link);
       link.click();
      }
    );
}

function loginToApiCall(startDateTS, endDateTS, categoriesIds, storeId) {
  return dispatch => {
    const config = {
       method:"POST",
       url: `${apiConstants.DM_API_URL}/login-dm`,
       data: {
          'dmcred':'YWNjb3VudHNAZGVsaXZlcnltYW4uY29tLmN5JCQlJSQkeHpNZU1kejM2cm1jWTg4ViQkJSUkJGFzRUZmY2RzZ2RTRkhHU0E='
        }
     };

     config.headers = {
        'content-type': 'application/json',
     };

     axios.request(config).then(
      response =>{
        localStorage.setItem('dm-api-token', response["data"]["access_token"]);
        }
      );
    }

}
