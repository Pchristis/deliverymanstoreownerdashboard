import { storesConstants } from '../constants';
import { getCookie } from '../utils/cookies';
import { apiConstants } from '../constants';

import axios from 'axios';

export const storesActions = {
    getStores,
    filterCategories
};

function getStores() {
    return { type: storesConstants.STORES_REQUEST };
}


function filterCategories(store) {
  return dispatch => {
    const config = {
       method:"GET",
       url: `${apiConstants.BASE_URL}/api/getCategories?lang=el&shopId=${store}`,
     };

     config.headers = {
        'content-type': 'application/json',
        'Authorization': `Bearer ${getCookie('token')}`
     };

     axios.request(config).then(
      response =>{
            dispatch(success(response.data.Categories));
        }
      );
    }

    function success(categories) { return { type: storesConstants.FILTER_CATEGORIES, categories } }

}
