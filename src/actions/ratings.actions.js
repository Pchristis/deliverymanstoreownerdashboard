import { ratingsConstants } from '../constants';
import { history } from '../helpers';
import { apiConstants } from '../constants';
import { getCookie } from '../utils/cookies';

import axios from 'axios';


export const ratingsActions = {
    filterRatings
};

function filterRatings(storeId,ratingMoreThan,lowToHigh) {
  let extraUrl = ''
  if (ratingMoreThan > 0){
    extraUrl += '&stars='+ratingMoreThan;
  }
  if (lowToHigh == 0){
    extraUrl += '&lowToHigh=true';
  }else{
    extraUrl += '&highToLow=true';
  }
  return dispatch => {
    const config = {
       method:"GET",
       url: `${apiConstants.BASE_URL}/api/getRestaurantRates?&storeId=${storeId}${extraUrl}`,
     };

     config.headers = {
        'content-type': 'application/json',
        'Authorization': `Bearer ${getCookie('token')}`
     };

     axios.request(config).then(
      response =>{
          dispatch(success(response.data));
        }
      );
    }

    function success(ratings) { return { type: ratingsConstants.RATINGS_SUCCESS, ratings } }

}
