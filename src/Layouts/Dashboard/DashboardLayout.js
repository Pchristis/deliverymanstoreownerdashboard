import React, { Component } from 'react';
import { Route, Redirect } from 'react-router-dom';

import { checkCookie } from '../../utils/cookies';
import { MainHeader } from './components/MainHeader';
import { SideBar } from './components/SideBar';


import 'primereact/resources/themes/nova-light/theme.css';
import 'primereact/resources/primereact.min.css';
import 'primeicons/primeicons.css';
import 'bootstrap/dist/css/bootstrap.css';
import 'bootstrap-daterangepicker/daterangepicker.css';

const DashboardLayout = ({children, ...rest}) => {
  return (
    <div className="wrapper">
      <MainHeader/>
      <SideBar/>
      <div className="content-wrapper">
        <section className="content" id="component-page">{children}</section>
      </div>
    </div>
  )
}

const DashboardLayoutRoute = ({component: Component, ...rest}) => {
  return (
    <Route {...rest} render={matchProps => (
        checkCookie() !== null
            ? <DashboardLayout><Component {...matchProps} /></DashboardLayout>
            : <Redirect to={{ pathname: '/login', state: { from: matchProps.location } }} />
    )} />
  )
};

export default DashboardLayoutRoute;
