import React from 'react';
import { Link } from 'react-router-dom';
import { connect } from 'react-redux';

import { userActions } from '../../../actions';


class MainHeader extends React.Component {

    constructor(props) {
      super(props);
      const { dispatch } = this.props;

      dispatch(userActions.getUserAuth());
      dispatch(userActions.getUserInfo());
    }

    render() {

        const { userinfo } = this.props;
        const { user } = this.props.user;

        return (
          <header className="main-header">
            <Link to="/overview" className="logo">
            <span className="logo-mini"><b>DM</b></span>
            <span className="logo-lg"><img src="/assets/images/logo-white.svg" alt="DeliveryMan" id="login-logo"/></span>
          </Link>
          <nav className="navbar navbar-static-top">
            <Link to="#" className="sidebar-toggle" data-toggle="push-menu" role="button">
              <span className="sr-only">Toggle navigation</span>
              <span className="icon-bar"></span>
              <span className="icon-bar"></span>
              <span className="icon-bar"></span>
            </Link>
            {userinfo &&
            <p className="headerWelcome">{user && <img src={user.picture}/>}Welcome {userinfo.name} {userinfo.surname}</p>
            }
            <div className="navbar-custom-menu">
              <ul className="nav navbar-nav">
                <li>
                  <Link to="/settings"><i className="fa fa-cog" aria-hidden="true"></i> Settings</Link>
                </li>
                <li>
                  <Link to="/login"><i className="fa fa-sign-out-alt" aria-hidden="true"></i> Sign Out</Link>
                </li>
              </ul>
            </div>
          </nav>
        </header>
        )
      }
}


function mapStateToProps(state) {
    const { userinfo } = state;
    const { user } = userinfo;

    return {
      user: state.authentication, userinfo
    };
}

const connectedMainHeader = connect(mapStateToProps)(MainHeader);
export { connectedMainHeader as MainHeader };
