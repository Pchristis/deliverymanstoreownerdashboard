import React from 'react';
import { Link } from 'react-router-dom';
import { connect } from 'react-redux';

import { userActions } from '../../../actions';

class SideBar extends React.Component {

constructor(props) {
  super(props);
  const { dispatch } = this.props;

  dispatch(userActions.getUserAuth());
  dispatch(userActions.getUserInfo());
}

render() {

  const { userinfo } = this.props;
  const { user } = this.props.user;

  return (
    <aside className="main-sidebar">
    <section className="sidebar">
      <ul className="sidebar-menu" data-widget="tree">
        <li className="mobile-visibility-only logo-menu-item">
          <Link to="/">
            <span className="logo-lg"><img src="/assets/images/logo-white.svg" alt="DeliveryMan" id="login-logo"/></span>
          </Link>
        </li>
        <li className="active">
          <Link to="/overview">
            <i className="fa fa-chart-area"></i>
            <span>Overview</span>
          </Link>
        </li>
        <li>
          <Link to="/analysis">
            <i className="fa fa-file-alt"></i>
            <span>Analysis</span>
          </Link>
        </li>
        {!userinfo.dashboardViewOnly ? (
          <li>
            <Link to="/menu">
              <i className="fa fa-utensils"></i>
              <span>Menu / Catalog</span>
            </Link>
          </li>
        ):''}
        {!userinfo.dashboardViewOnly ? (
          <li>
            <Link to="/storecontrol">
              <i className="fa fa-sliders-h"></i>
              <span>Store Control</span>
            </Link>
          </li>
        ):''}
        <li>
          <Link to="/invoices">
            <i className="fa fa-list-alt"></i>
            <span>Invoices</span>
          </Link>
        </li>
        <li>
          <Link to="/ratings">
            <i className="fa fa-star"></i>
            <span>Ratings</span>
          </Link>
        </li>
        <li className="mobile-visibility-only">
          <Link to="/settings">
            <i className="fa fa-cog"></i>
            <span>Settings</span>
          </Link>
        </li>
        <li className="mobile-visibility-only">
          <Link to="/login">
            <i className="fa fa-sign-out-alt"></i>
            <span>Sign Out</span>
          </Link>
        </li>
      </ul>
    </section>
  </aside>
  )
}
}

function mapStateToProps(state) {
    const { userinfo } = state;
    const { user } = userinfo;

    return {
      user: state.authentication, userinfo
    };
}

const connectedSideBar = connect(mapStateToProps)(SideBar);
export { connectedSideBar as SideBar };
