import axios from 'axios';
import {apiConstants} from '../constants/api.constants';
import { getCookie } from '../utils/cookies';

export const performRequest = (method, api, params, auth, timeout=180000) => {
  const body = method === 'get' ? 'params' : 'data';
  let baseURL = apiConstants.BASE_URL;

  if (baseURL.includes("-dev") && api.includes('doIndexSearch'))
    api =  api.replace("doIndexSearch", "doIndexSearchdev");

   const config = {
    method,
    url: "api/" + api,
    baseURL: baseURL,
    [body]: params || {},
    timeout:timeout
  };
  if (auth) {
    config.headers = {
        'Authorization': `Bearer ${getCookie('token')}`
    };
  }
  return axios.request(config);
};

