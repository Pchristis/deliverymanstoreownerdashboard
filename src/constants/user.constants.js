export const userConstants = {
    FORGOT_REQUEST: 'USERS_FORGOT_PASSWORD_REQUEST',
    LOGIN_REQUEST: 'USERS_LOGIN_REQUEST',
    LOGIN_SUCCESS: 'USERS_LOGIN_SUCCESS',
    IS_LOGGED_IN: 'USER_IS_LOGGED_IN',
    LOGIN_FAILURE: 'USERS_LOGIN_FAILURE',
    UPDATE_USER_INFO: 'UPDATE_USER_INFO',

    LOGOUT: 'USERS_LOGOUT'

};
