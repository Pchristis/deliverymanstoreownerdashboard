import React from 'react';

export function renderModalDateInput(field) {
  const {
    meta: { touched, error }
  } = field;

  const className = `form-group ${touched && error ? 'has-showModal' : ''}`;
  const fieldIputs = {
    ...field.input,
    value: field.valueFromState
  };
  if (field.label) {
    return (
      <div className={className}>
        <label>{field.label}</label>
        <input
          className="form-control"
          type="date"
          disabled={field.disabled}
          {...fieldIputs}
        />
        <div className="text-danger">{touched ? error : ''}</div>
      </div>
    )
  }else{
    return (
      <div className={className}>
        <input
          className="form-control"
          type="date"
          disabled={field.disabled}
          {...fieldIputs}
        />
        <div className="text-danger">{touched ? error : ''}</div>
      </div>
    )
  }
}
