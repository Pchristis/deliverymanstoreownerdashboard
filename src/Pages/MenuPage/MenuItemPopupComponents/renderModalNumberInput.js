import React from 'react';

export function renderModalNumberInput(field) {
  const {
    meta: { touched, error }
  } = field;

  const className = `form-group ${touched && error ? 'has-showModal' : ''}`;
  const fieldIputs = {
    ...field.input,
    value: field.valueFromState
  };
  return (
    <div className={className}>
      <label>{field.label}</label>
      <input
        className="form-control"
        type="number"
        step="1"
        min="0"
        disabled={field.disabled}
        {...fieldIputs}
      />
      <div className="text-danger">{touched ? error : ''}</div>
    </div>
  );
}
