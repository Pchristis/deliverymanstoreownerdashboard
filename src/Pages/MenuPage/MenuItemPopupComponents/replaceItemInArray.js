export default function replaceItemInArrayByKey(collection, replacement, key) {
    const index = collection.findIndex(a => a[key] === replacement[key]);
    collection.splice(index, 1, replacement);
    return collection;
  }
  
  export function replaceItemInArrayByIndex(collection, replacement, index) {
    replacement
      ? collection.splice(index, 1, replacement)
      : collection.splice(index, 1);
    return collection;
  }
  