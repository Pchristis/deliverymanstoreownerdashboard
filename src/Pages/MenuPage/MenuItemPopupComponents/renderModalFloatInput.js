import React from 'react';

export function renderModalFloatInput(field) {
  const {
    meta: { touched, error }
  } = field;

  const className = `form-group ${touched && error ? 'has-showModal' : ''}`;
  const fieldIputs = {
    ...field.input,
    value: field.hasOwnProperty("valueFromState")?field.valueFromState:''
  };
  if (field.label) {
    return (
      <div className={className}>
        <label>{field.label}</label>
        <input
          className="form-control"
          type="number"
          step={field.step?field.step:"0.01"}
          min="0"
          disabled={field.disabled}
          {...fieldIputs}
        />
        <div className="text-danger">{touched ? error : ''}</div>
      </div>
    );
  } else {
    return (
      <div className={className}>
        <input
          className="form-control"
          type="number"
          step="0.01"
          min="0"
          disabled={field.disabled}
          {...fieldIputs}
        />
        <div className="text-danger">{touched ? error : ''}</div>
      </div>
    );
  }
}
