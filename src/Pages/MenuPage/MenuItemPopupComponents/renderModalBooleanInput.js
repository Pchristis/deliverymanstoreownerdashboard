import React from 'react';

export function renderModalBooleanInput(field) {
  const {
    meta: { touched, error }
  } = field;
  const className = `form-group ${touched && error ? 'has-showModal' : ''}`;
  const fieldIputs = {
    ...field.input
  };

  if (field.label) {
    return (
      <div className={className} >
        <label>{field.label}</label>
        <input
          className="form-control1"
          type="checkbox"
          disabled={field.disabled}
          {...fieldIputs}
          defaultChecked={field.valueFromState}
        />
        <div className="text-danger">{touched ? error : ''}</div>
      </div>
    );
  } else {
    return (
      <div className={className} >
        <input
          className="form-control"
          type="checkbox"
          disabled={field.disabled}
          {...fieldIputs}
          defaultChecked={field.valueFromState}
        />
        <div className="text-danger">{touched ? error : ''}</div>
      </div>
    );
  }
}
