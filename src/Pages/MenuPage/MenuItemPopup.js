import React, { Component } from 'react';
import {
  Badge,
  Button,
  Card,
  CardBody,
  CardHeader,
  Col,
  Modal,
  ModalBody,
  ModalFooter,
  ModalHeader,
  NavItem,
  NavLink,
  Row,
  Table
} from 'reactstrap';
import _ from 'lodash';

//Used in item edit
import { renderModalTextInput } from './MenuItemPopupComponents/renderModalTextInput';
import { renderModalBooleanInput } from './MenuItemPopupComponents/renderModalBooleanInput';
import { renderModalDateInput } from './MenuItemPopupComponents/renderModalDateInput';
import { renderModalNumberInput } from './MenuItemPopupComponents/renderModalNumberInput';
import { renderModalFloatInput } from './MenuItemPopupComponents/renderModalFloatInput';
import replaceItemInArray from './MenuItemPopupComponents/replaceItemInArray';

import { Field, reduxForm } from 'redux-form'; //?
import { connect } from 'react-redux'; //?
import axios from 'axios';
import { apiConstants } from '../../constants';
import Resizer from 'react-image-file-resizer';
import { getCookie } from '../../utils/cookies';


// import { uploadImage } from '../../../actions'; //Skip it
// import { IMAGE_CONTAINERS } from '../../../constants/backend';
// import savingGif from '../../../assets/img/ajax-loader.gif' //Skip it

//Used in item edit
const DEFAULT_BACKEND_LANGUAGE = {
    key: 'el',
    description: 'Greek/Ελληνικά'
};
const LANGUAGES = [
    { key: 'el', description: 'Greek/Ελληνικά' },
    { key: 'en', description: 'English/Αγγλικά' }
];

class MenuItemPopup extends Component {
  constructor(props, context) {
    super(props, context);
    this.resetModal = this.resetModal.bind(this);
    this.resetItem = this.resetItem.bind(this);

    this.switchLanguage = this.switchLanguage.bind(this);
    this.onFieldChange = this.onFieldChange.bind(this);
    this.onSizesFieldChange = this.onSizesFieldChange.bind(this);
    this.onDiscountFieldChange = this.onDiscountFieldChange.bind(this);
    this.onFieldAllergyChange = this.onFieldAllergyChange.bind(this);
    this.addDiscount = this.addDiscount.bind(this);
    this.addAllergy = this.addAllergy.bind(this);
    this.deleteAllergy = this.deleteAllergy.bind(this);
    this.deleteDiscount = this.deleteDiscount.bind(this);
    this.updateParentState = this.updateParentState.bind(this);
    this.addOption = this.addOption.bind(this);
    this.editOption = this.editOption.bind(this);

    const languageColSize = 12 / LANGUAGES.length;
    this.state = {
      defaultBackendLanguage: DEFAULT_BACKEND_LANGUAGE,
      selectedLanguage: DEFAULT_BACKEND_LANGUAGE,
      languageColSize,
      currentStateOfModal: false,
      item: null
    };
  }

  static getDerivedStateFromProps(props, state) {
    if (!state.currentStateOfModal && props.showMenuItemModal) {
        let item = _.cloneDeep(props.item)
        item.el.itemCode = item.el.alias
        item.en.itemCode = item.en.alias

        return { currentStateOfModal: true, item: item };
    }
    return null;
  }

  resetModal() {
    this.resetItem();
    this.props.closeShowMenuItemModal(
      this.props.category.alias
    );
  }

  resetItem() {
    this.setState({
      item: null,
      currentlyEditedOption: null,
      currentStateOfModal: false,
      selectedLanguage: DEFAULT_BACKEND_LANGUAGE
    });
  }

  updateParentState() {
    let item = this.state.item;
    console.log('updateparentstate before',_.cloneDeep(item));
    const itemAlias = this.state.item[this.state.defaultBackendLanguage.key].alias
    ? this.state.item[this.state.defaultBackendLanguage.key].alias
    : _.snakeCase(this.state.item[this.state.defaultBackendLanguage.key].name);

    const sortIndx =   _.has(item, `${this.state.defaultBackendLanguage.key}.sortIndex`)?item[this.state.defaultBackendLanguage.key].sortIndex:0;

    LANGUAGES.map(lang => {
      item[lang.key].alias = itemAlias;
      item[lang.key].sortIndex = sortIndx;
      if (_.has(item[lang.key], `sizePrices`) && item[lang.key].sizePrices.length>0)
        {
          item[lang.key].sizePrices = _.sortBy(item[lang.key].sizePrices,'sortingIndex');
        }
      else if (_.has(item[lang.key], `sizePrices`) && item[lang.key].sizePrices.length===0)
         delete  item[lang.key].sizePrices
    });

    console.log('updateparentstate after',_.cloneDeep(item));
    this.setState({ item });
    this.props.onMenuItemUpdate(item,this.props.category);
  }

  onFieldChange(langKey, event) {
   /* const value =
      event.target.type === 'checkbox'
        ? event.target.checked
        : event.target.type === 'number'
          ? parseFloat(event.target.value)
          : event.target.value;*/

          const value =
          event.target.type === 'checkbox'
            ? event.target.checked
            : (event.target.type === 'number' && event.target.name==='sortIndex')
              ?(parseFloat(event.target.value)||0)
              : event.target.type === 'number'
              ? parseFloat(event.target.value)
              :event.target.value;

    let item = _.cloneDeep(this.state.item);

    if (langKey) {
      _.set(item, `${langKey}.${event.target.name}`, value);
    } else {
      LANGUAGES.map(lang => {
        _.set(item, `${lang.key}.${event.target.name}`, value);
        if (event.target.name === 'price') {
          delete item[lang.key]['sizePrices'];
        }
         if (event.target.name === 'isNew' && event.target.checked ===false ) {
           delete item[lang.key]['isNew'];
         }
         if(event.target.name === 'deleteImage' ){
           delete item[lang.key]['thumbnail'];
            delete item[lang.key]['image'];
         }
          if (event.target.name === 'isPhCCompo' && event.target.checked ===false ) {
            delete item[lang.key]['isPhCCompo'];
          }
           if (event.target.name === 'refund_Replace' && event.target.checked ===false ) {
              delete item[lang.key]['refund_Replace'];
            }
          if (event.target.name === 'excludeFromOffers' && event.target.checked ===false ) {
            delete item[lang.key]['excludeFromOffers'];
          }
          if (event.target.name === 'commissionRate' && event.target.value===""){
            delete item[lang.key][event.target.name];
          }
      });
    }


    this.setState({
      item: item
    });
  }

  checkUncheckAllOptions = (event,options,propertyName) =>
  {
   let currentlyEditedOption = _.cloneDeep(this.state.currentlyEditedOption);
    // console.log('checkUncheckAllOptions',options,currentlyEditedOption,propertyName)

     options.map(opt =>{
          LANGUAGES.map(lang => {
                const currentOption = {
                  ...currentlyEditedOption[lang.key].optionItems.find(
                    oi => oi.alias === opt.alias
                  ),
                  [propertyName]: event.target.checked
                };

                replaceItemInArray(
                  currentlyEditedOption[lang.key].optionItems,
                  currentOption,
                  'alias'
                );
              });
     })

   this.setState({ currentlyEditedOption });
  }

  checkifAllSelected = (options,propertyName) =>{
     let currentlyEditedOption = _.cloneDeep(this.state.currentlyEditedOption);

     if(currentlyEditedOption.el.templateOptionSelect==="")
       return false

     const optionsLen = options.length;
     const selectedOptions = options.filter(opt=>opt[propertyName]);

     if (optionsLen==selectedOptions.length)
       return true
     return false
  }

  checkIfAllOptionsHaveTheSameSubMenu = (options) =>{

   let currentlyEditedOption = _.cloneDeep(this.state.currentlyEditedOption);

       if(currentlyEditedOption.el.templateOptionSelect==="" || options.length===0)
         return ""

       const tmp = options[0].phcSubMenu||""; // get for the 1st option the phcSubMenu to compare it with the rest options
       /*check if there is one option that has different phcSubMenu value from the first item...
       if true then we return empty because that measn that not all option belong to the same submenu. if false that means that all option belong to the same submenu*/

      const res =  options.some(opt=> opt.phcSubMenu !== tmp ) ;

      if (res)
         return ''

       return tmp

  }

  setSubMenuForAllOptions= (event,options) =>{
    let currentlyEditedOption = _.cloneDeep(this.state.currentlyEditedOption);
    options.map(opt =>{
         LANGUAGES.map(lang => {
                    let currentOption = {
                      ...currentlyEditedOption[lang.key].optionItems.find(
                        oi => oi.alias === opt.alias
                      ),
                      "phcSubMenu": event.target.value *1
                    };

                    if ( event.target.value.trim()==="" ||  event.target.value===undefined ||  event.target.value ===null)
                       delete  currentOption.phcSubMenu
                    replaceItemInArray(
                      currentlyEditedOption[lang.key].optionItems,
                      currentOption,
                      'alias'
                    );
                  });
         })

       this.setState({ currentlyEditedOption });
      }


  _excludeOptionFromSize = (event,sizeAlias) =>{
    let currentlyEditedOption = _.cloneDeep(this.state.currentlyEditedOption);

    LANGUAGES.map(lang => {
      if (event.target.checked) // add this option to the array
       {
         currentlyEditedOption[lang.key].hiddenForSizes?currentlyEditedOption[lang.key].hiddenForSizes.push(sizeAlias):currentlyEditedOption[lang.key].hiddenForSizes=[sizeAlias]
       }
      else
       {
         const ind = currentlyEditedOption[lang.key].hiddenForSizes.indexOf(sizeAlias)
         currentlyEditedOption[lang.key].hiddenForSizes.splice(ind,1);
           if (currentlyEditedOption[lang.key].hiddenForSizes.length===0)
              delete currentlyEditedOption[lang.key].hiddenForSizes;
       }
    });

    this.setState({ currentlyEditedOption });

  }

  onOptionLiveTableFieldChange(alias, event) {
    let currentlyEditedOption = _.cloneDeep(this.state.currentlyEditedOption);
   const value =
             event.target.type === 'checkbox'
               ? event.target.checked
               : event.target.value;

    LANGUAGES.map(lang => {
      const currentOption = {
        ...currentlyEditedOption[lang.key].optionItems.find(
          oi => oi.alias === alias
        ),
        [event.target.name]: value
      };

      replaceItemInArray(
        currentlyEditedOption[lang.key].optionItems,
        currentOption,
        'alias'
      );
    });

    this.setState({ currentlyEditedOption });
  }

  onFieldAllergyChange(langKey, event) {
    let currentlyEditedAllergy = this.state.currentlyEditedAllergy || {};
    if (event.target.value === '') {
      delete currentlyEditedAllergy[langKey];
      this.setState({ currentlyEditedAllergy });
    } else {
      _.set(
        currentlyEditedAllergy,
        `${langKey}.${event.target.name}`,
        event.target.value
      );

      this.setState({ currentlyEditedAllergy });
    }
  }

  addAllergy() {
    let item = this.state.item;
    LANGUAGES.map(lang => {
      const allergies = _.union(this.state.item[lang.key].allergies || [], [
        this.state.currentlyEditedAllergy[lang.key].allergy
      ]);

      _.set(item, `${lang.key}.allergies`, allergies);
    });

    this.setState({
      item: item,
      currentlyEditedAllergy: {}
    });
  }

  deleteAllergy(allergyIndex, event) {
    let item = _.cloneDeep(this.state.item);
    LANGUAGES.map(lang => {
      item[lang.key].allergies.splice(allergyIndex, 1);
    });
    this.setState({ item });
  }

  deleteDiscount(discountIndex) {
    let item = _.cloneDeep(this.state.item);
    LANGUAGES.map(lang => {
      item[lang.key].discount.splice(discountIndex, 1);
    });
    this.setState({ item });
  }

  switchLanguage(language) {
    const defaultLanguageIngredient = this.state.item[
      this.state.defaultBackendLanguage.key
    ];
    let newItem = this.state.item;

    LANGUAGES.map(lang => {
      const langInfo = this.state.item[lang.key];
      return _.isEqual(lang, this.state.defaultBackendLanguage)
        ? _.set(newItem, `${lang.key}`, langInfo)
        : _.set(
            newItem,
            `${lang.key}`,
            _.assign({}, defaultLanguageIngredient, langInfo)
          );
    });

    // setState takes a callback which allows you to ensure the code
    // after is called only after this.state is updated.
    this.setState({ selectedLanguage: language, item: newItem });
  }

  /*renderNavItems() {
    return LANGUAGES.map(language => {
      return (
        <NavItem key={language.key}>
          <NavLink
            onClick={() => this.switchLanguage(language)}
            active={this.state.selectedLanguage.key === language.key}
            disabled={this.state.selectedLanguage.key === language.key}>
            {language.description}
          </NavLink>
        </NavItem>
      );
    });
  }*/

  renderItemsBasicInfo() {
    return (
      <div>
        <Row>
          {LANGUAGES.map((lang, i) => {
            return (
              <Col key={i} xs="12" md={6}>
                <Field
                  label={`${lang.description} Item Name`}
                  name="name"
                  disabled={false}
                  valueFromState={_.get(
                    this.state.item,
                    `${lang.key}.name`,
                    ''
                  )}
                  component={renderModalTextInput}
                  onChange={event => this.onFieldChange(lang.key, event)}
                />
              </Col>
            );
          })}
          {/* <Col xs="12" md={2}>
            <Field
              label="Commission Rate"
              name="commissionRate"
              disabled={false}
              valueFromState={_.get(
                this.state.item,
                `${this.state.defaultBackendLanguage.key}.commissionRate`,
                ''
              )}
              component={renderModalFloatInput}
              onChange={event => this.onFieldChange(null, event)}
            />
          </Col> */}
          {/* <Col xs="12" md={2}>
            <Field
              label="Sort Position"
              name="sortIndex"
              disabled={false}
              valueFromState={_.get(
                this.state.item,
                `${this.state.defaultBackendLanguage.key}.sortIndex`,
                ''
              )}
              component={renderModalNumberInput}
              onChange={event => this.onFieldChange(null, event)}
            />
          </Col> */}

        </Row>
        <Row>
          {LANGUAGES.map((lang, i) => {
            return (
              <Col key={i} xs="12" md={6}>
                <Field
                  label={`${lang.description} Item Description`}
                  name="description"
                  disabled={false}
                  valueFromState={_.get(
                    this.state.item,
                    `${lang.key}.description`,
                    ''
                  )}
                  component={renderModalTextInput}
                  onChange={event => this.onFieldChange(lang.key, event)}
                />
              </Col>
            );
          })}
          {/* <Col xs="12" md={2}>
            <Field
              label="Vat"
              name="vat"
              disabled={false}
              valueFromState={_.get(
                this.state.item,
                `${this.state.defaultBackendLanguage.key}.vat`,
                ''
              )}
              component={renderModalFloatInput}
              onChange={event => this.onFieldChange(null, event)}
            />
          </Col> */}
        </Row>
        <Row>
         {/* <Col xs="12" md={1}>
            <Field
              label="Active"
              name="active"
              disabled={false}
              valueFromState={_.get(
                this.state.item,
                `${this.state.defaultBackendLanguage.key}.active`,
                false
              )}
              component={renderModalBooleanInput}
              onChange={event => this.onFieldChange(null, event)}
            />
          </Col>
          <Col xs="12" md={1}>
            <Field
              label="Is New"
              name="isNew"
              className="checkbox-black"
              disabled={false}
              valueFromState={_.get(
                this.state.item,
                `${this.state.defaultBackendLanguage.key}.isNew`,
                false
              )}
              component={renderModalBooleanInput}
              onChange={event => this.onFieldChange(null, event)}
            />
          </Col> */}
          {/* <Col xs="12" md={2}>
            <Field
              label="Exclude From Offers"
              name="excludeFromOffers"
              disabled={false}
              valueFromState={_.get(
                this.state.item,
                `${this.state.defaultBackendLanguage.key}.excludeFromOffers`,
                false
              )}
              component={renderModalBooleanInput}
              onChange={event => this.onFieldChange(null, event)}
            />
          </Col> */}
          {/* <Col xs="12" md={2}>
            <Field
              label="Item Code"
              name="itemCode"
              valueFromState={_.get(
                this.state.item,
                `${this.state.defaultBackendLanguage.key}.itemCode`,
                ''
              )}
               component={renderModalTextInput}
              onChange={event => this.onFieldChange(null, event)}
            />
          </Col> */}
           {/* <Col xs="12" md={2}>
            <Field
              label="Is PHC Compo"
              name="isPhCCompo"
              disabled={false}
              valueFromState={_.get(
                this.state.item,
                `${this.state.defaultBackendLanguage.key}.isPhCCompo`,
                false
              )}
              component={renderModalBooleanInput}
              onChange={event => this.onFieldChange(null, event)}
            />
          </Col> */}
           {/* <Col xs="12" md={2}>
              <Field
                label="Refund/Replace Item"
                name="refund_Replace"
                disabled={false}
                valueFromState={_.get(
                  this.state.item,
                  `${this.state.defaultBackendLanguage.key}.refund_Replace`,
                  false
                )}
                component={renderModalBooleanInput}
                onChange={event => this.onFieldChange(null, event)}
              />
            </Col> */}
        </Row>
      </div>
    );
  }

  renderSizePrices() {
    console.log("Price State",this.state)
    if (
      _.has(
        this.props.category.sizes
      )
    ) {
      const sizeFields = this.props.category.sizes.map((size, i) => {
        return (
          <Col
            xs="12"
            md={8}
            //   8 /
            //   this.props.category.sizes.length
            // }
            key={i}>
            <Field
              key={i}
              label={size.size}
              name={size.alias}
              disabled={false}
              valueFromState={
                (
                  (
                    (
                      (this.state.item || {})[
                        this.state.selectedLanguage.key
                      ] || {}
                    ).sizePrices || [{}]
                  ).find(sz => sz.alias === size.alias) || {}
                ).price || ''
              }
              component={renderModalFloatInput}
              onChange={e => this.onSizesFieldChange(i, e)}
            />
          </Col>
        );
      });
      return (
        <Col xs="12" md="3">
          <Card>
            <CardHeader className="header-black">Price</CardHeader>
            <CardBody>
              <Row>
                <Col>
                  <Field
                    label="Price"
                    name="price"
                    disabled={false}
                    valueFromState={
                      (
                        (this.state.item || {})[
                          this.state.selectedLanguage.key
                        ] || {}
                      ).price || '0'
                    }
                    component={renderModalFloatInput}
                    onChange={event => this.onFieldChange(null, event)}
                  />
                </Col>
                {sizeFields}
              </Row>
            </CardBody>
          </Card>
        </Col>
      );
    } else {
      return (
        <div>
          <Row>
          <Col xs={12} sm={6} md={6}>
            <Card>
              <CardHeader className="header-black">Price</CardHeader>
              <CardBody>
                  <Field
                    name="price"
                    disabled={false}
                    valueFromState={
                      ((this.state.item || {})[this.state.selectedLanguage.key] || {})
                        .price || '0'
                    }
                    component={renderModalFloatInput}
                    onChange={event => this.onFieldChange(null, event)}
                  />
              </CardBody>
            </Card>
           </Col>
           {this.renderImages()}
          </Row>
        </div>
      );
    }
  }

  renderDiscounts() {
    return (
      <Card>
        <CardHeader className="header-black">Discount</CardHeader>
        <CardBody>
          <Row>
            <Col xs="12" md="3">
              <Field
                label="Percentage"
                name="percentage"
                disabled={
                  !_.isEqual(
                    this.state.defaultBackendLanguage.key,
                    this.state.selectedLanguage.key
                  )
                }
                valueFromState={_.get(
                  this.state.currentlyEditedDiscount,
                  'percentage',
                  ''
                )}
                component={renderModalFloatInput}
                onChange={this.onDiscountFieldChange}
              />
            </Col>
            <Col xs="12" md="3">
              <Field
                label="From Date"
                name="from"
                disabled={
                  !_.isEqual(
                    this.state.defaultBackendLanguage.key,
                    this.state.selectedLanguage.key
                  )
                }
                valueFromState={_.get(
                  this.state.currentlyEditedDiscount,
                  'from',
                  ''
                )}
                component={renderModalDateInput}
                onChange={this.onDiscountFieldChange}
              />
            </Col>
            <Col xs="12" md="3">
              <Field
                label="To Date"
                name="to"
                disabled={
                  !_.isEqual(
                    this.state.defaultBackendLanguage.key,
                    this.state.selectedLanguage.key
                  )
                }
                valueFromState={_.get(
                  this.state.currentlyEditedDiscount,
                  'to',
                  ''
                )}
                component={renderModalDateInput}
                onChange={this.onDiscountFieldChange}
              />
            </Col>
            <Col xs="12" md="2">
              <Field
                label="Active"
                name="active"
                disabled={
                  !_.isEqual(
                    this.state.defaultBackendLanguage.key,
                    this.state.selectedLanguage.key
                  )
                }
                valueFromState={_.get(
                  this.state.currentlyEditedDiscount,
                  'active',
                  false
                )}
                component={renderModalBooleanInput}
                onChange={this.onDiscountFieldChange}
              />
            </Col>
            <Col xs="12" md="1">
              <label />
              <Button
                className="float-right"
                color="danger"
                disabled={
                  !this.state.currentlyEditedDiscount ||
                  Object.keys(this.state.currentlyEditedDiscount).length !== 4
                }
                onClick={this.addDiscount}>
                Add
              </Button>
            </Col>
          </Row>
          {this.renderDiscountsAlreadyInserted()}
        </CardBody>
      </Card>
    );
  }

  renderDiscountsAlreadyInserted() {
    if (
      !_.has(
        this.state.item,
        `${this.state.defaultBackendLanguage.key}.discount`
      )
    )
      return null;

    return this.state.item[this.state.defaultBackendLanguage.key].discount.map(
      (disc, discIndex) => {
        return (
          <Row key={discIndex}>
            <Col xs="12" md="3">
              <Field
                name="percentage"
                disabled={true}
                valueFromState={disc.percentage}
                component={renderModalTextInput}
              />
            </Col>
            <Col xs="12" md="3">
              <Field
                name="from"
                disabled={true}
                valueFromState={disc.from}
                component={renderModalDateInput}
              />
            </Col>
            <Col xs="12" md="3">
              <Field
                name="to"
                disabled={true}
                valueFromState={disc.to}
                component={renderModalDateInput}
              />
            </Col>
            <Col xs="12" md="1">
              <Field
                name="active"
                disabled={true}
                valueFromState={disc.active}
                component={renderModalBooleanInput}
              />
            </Col>
            <Col xs="12" md="2">
              <Button
                className="float-right"
                color="danger"
                disabled={false}
                onClick={() => this.deleteDiscount(discIndex)}>
                DELETE
              </Button>
            </Col>
          </Row>
        );
      }
    );
  }

  onSizesFieldChange(index, event) {
    let item = _.cloneDeep(this.state.item);

    if (event.target.value === '' || event.target.value==0) {

      LANGUAGES.map(lang => {
        if (!item[lang.key].sizePrices)
          return;

        const index = item[lang.key].sizePrices.findIndex(
          a => a['alias'] === event.target.name
        );

        item[lang.key].sizePrices.splice(index, 1);
      });
    } else {
      // For each Language update size
      LANGUAGES.map(lang => {
        delete item[lang.key]['price'];

        const templateSizeIndex = this.props.category.sizes.findIndex(
          a => a['alias'] === event.target.name
        );
        // const index = item[lang.key].sizePrices.findIndex(
        //   a => a['alias'] === event.target.name
        // );

        const sizePrices = _.unionBy(
          [
            {
              alias: event.target.name,
              price: parseFloat(event.target.value),
              size: this.props.category.sizes[templateSizeIndex].size,
              sortingIndex: this.props.category.sizes[templateSizeIndex].sortingIndex,
              sizePieces: this.props.category.sizes[templateSizeIndex].sizePieces,
              sizeLength:this.props.category.sizes[templateSizeIndex].sizeLength,
              langKey:this.props.category.sizes[templateSizeIndex].langKey
            }
          ],
          item[lang.key].sizePrices,
          'alias'
        );
        _.set(item, `${lang.key}.sizePrices`, sizePrices);
      });

    }
     this.setState({ item });
  }

  onDiscountFieldChange(event) {
    const value =
      event.target.type === 'checkbox'
        ? event.target.checked
        : event.target.type === 'number'
          ? parseFloat(event.target.value)
          : event.target.value;

    if (value === '') {
      let currentlyEditedDiscount = this.state.currentlyEditedDiscount;
      delete currentlyEditedDiscount[event.target.name];
      this.setState({ currentlyEditedDiscount });
    } else {
      const currentlyEditedDiscount = {
        ...this.state.currentlyEditedDiscount,
        [event.target.name]: value
      };

      this.setState({ currentlyEditedDiscount });
    }
  }

  addDiscount() {
    let item = this.state.item;
    LANGUAGES.map(lang => {
      const discount = _.union(item[lang.key].discount || [], [
        this.state.currentlyEditedDiscount
      ]);
      _.set(item, `${lang.key}.discount`, discount);
    });

    this.setState({ item, currentlyEditedDiscount: {} });
  }

  renderAllergies() {
    return (
      <Card>
        <CardHeader className="header-black">Allergies</CardHeader>
        <CardBody>
          <Row>
            {LANGUAGES.map((lang, i) => {
              return (
                <Col key={i} xs="12" md={10 / LANGUAGES.length}>
                  <Field
                    label={`${lang.description} Allergy`}
                    name="allergy"
                    disabled={false}
                    valueFromState={_.get(
                      this.state.currentlyEditedAllergy,
                      `${lang.key}.allergy`,
                      ''
                    )}
                    component={renderModalTextInput}
                    onChange={event =>
                      this.onFieldAllergyChange(lang.key, event)
                    }
                  />
                </Col>
              );
            })}
            <Col xs="12" md="1" />
            <Col xs="12" md="1">
              <label />
              <Button
                className="float-right"
                color="danger"
                disabled={
                  !this.state.currentlyEditedAllergy ||
                  Object.keys(this.state.currentlyEditedAllergy).length !==
                    LANGUAGES.length
                }
                onClick={this.addAllergy}>
                Add
              </Button>
            </Col>
          </Row>
          {this.renderAllergiesAlreadyInserted()}
        </CardBody>
      </Card>
    );
  }


   resizeFileThumbnail = (file) => new Promise(resolve => {
     Resizer.imageFileResizer(
               file,
               256,
               256,
               'JPEG',
               100,
               0,
               uri => {
                 resolve(this.setState({uri}));
               },
               'blob',
      );
 });

 fileChangedHandlerThumbnail = async (event) => {
 console.log('event',event)

   const file = event;
   const image = await this.resizeFileThumbnail(file);
   let item = this.state.item;
   let data = new FormData();
   let msg='';
   data.append('info', JSON.stringify({containerToSave:'menuitemsphotos'}));
   data.append('logo', this.state.uri);

    const config = {
              method:"POST",
              url: 'https://deliveryman-auth.azurewebsites.net/api/saveImgTo',
              data,
            };

            config.headers = {
               'content-type': 'application/json',
               'Authorization':`Bearer ${getCookie('token')}`
            };

            axios.request(config).then(
                 res =>{
                  LANGUAGES.map(lang => {

                       _.set(
                       item,
                       `${lang.key}.thumbnail`,
                       res.data.fileName
                      );});
                      this.setState({
                       item
                     });
                  })
                  .catch(err=>{
                          console.log(err);
                          msg='Image Not uploaded';
                         this.setState({...this.state,actionResultMessage:msg});
                  })
 }

  resizeFileImage = (file) => new Promise(resolve => {
       Resizer.imageFileResizer(
                 file,
                 1280,
                 1280,
                 'JPEG',
                 100,
                 0,
                 uri => {
                   resolve(this.setState({uri}));
                 },
                 'blob'
        );
  });

  fileChangedHandlerImage = async (event) => {
     const file = event.target.files[0];
     const image = await this.resizeFileImage(file);
     let item = this.state.item;
     let data = new FormData();
     let msg='';
     data.append('info', JSON.stringify({containerToSave:'menuitemsphotos'}));
     data.append('logo', this.state.uri);

     const config = {
            method:"POST",
            url: 'https://deliveryman-auth.azurewebsites.net/api/saveImgTo',
            data,
          };

          config.headers = {
             'content-type': 'application/json',
             'Authorization':`Bearer ${getCookie('token')}`
          };

          axios.request(config).then(
           res =>{
                   LANGUAGES.map(lang => {
                         _.set(
                         item,
                         `${lang.key}.image`,
                         res.data.fileName
                        );
                   });
                   this.setState({
                      item
                   },()=>this.fileChangedHandlerThumbnail(file));
           }
           )
           .catch(err=>{
                    console.log(err);
                    msg='Image Not uploaded';
                   this.setState({...this.state,actionResultMessage:msg});
           })

  }

  renderAllergiesAlreadyInserted() {
    if (!this.state.item) return null;
    if (!this.state.item[this.state.defaultBackendLanguage.key].allergies)
      return null;

    return this.state.item[this.state.defaultBackendLanguage.key].allergies.map(
      (allergy, i) => {
        return (
          <Row key={i}>
            {LANGUAGES.map((lang, langIndex) => {
              return (
                <Col key={langIndex} xs="12" md={10 / LANGUAGES.length}>
                  <Field
                    name="allergy"
                    disabled={true}
                    valueFromState={this.state.item[lang.key].allergies[i]}
                    component={renderModalTextInput}
                  />
                </Col>
              );
            })}
            <Col xs="12" md="2">
              <Button
                className="float-right"
                color="danger"
                disabled={false}
                onClick={event => this.deleteAllergy(i, event)}>
                DELETE
              </Button>
            </Col>
          </Row>
        );
      }
    );
  }
  onOptionFieldChange=(langKey, event)=> {

   /* const value =
      event.target.type === 'checkbox'
        ? event.target.checked
        : event.target.type === 'number'
          ? parseFloat(event.target.value)
          : event.target.value;*/

          const value =
          event.target.type === 'checkbox'
            ? event.target.checked
            : (event.target.type === 'number' && event.target.name === 'sortIndex')
              ? (parseFloat(event.target.value)||0)
              : event.target.type === 'number'
              ? parseFloat(event.target.value)
              : event.target.value;

    let currentlyEditedOption = _.cloneDeep(
      this.state.currentlyEditedOption || {}
    );

    if (langKey) {
      _.set(currentlyEditedOption, `${langKey}.${event.target.name}`, value);
    } else {
      LANGUAGES.map(lang => {
        _.set(currentlyEditedOption, `${lang.key}.${event.target.name}`, value);
         if (event.target.name==="quantitySelection" && value===false)
          delete currentlyEditedOption[lang.key][event.target.name]

        if (event.target.name === 'templateOptionSelect') {
            currentlyEditedOption[lang.key] =_.pick(currentlyEditedOption[lang.key],["templateOptionSelect"])
          _.set(currentlyEditedOption, `${lang.key}.optionItems`, []);
          if (event.target.value) {
            const templateIngredients = this.props.category.templates.find(templ => templ.alias === event.target.value)
              .ingredients||[];
            const optionItems = templateIngredients.map(option => {
              return { ...option, active: false, included: false };
            });
            _.set(
              currentlyEditedOption,
              `${lang.key}.optionItems`,
              optionItems
            );

          }
        }
      });
    }

    this.setState({
      currentlyEditedOption
    });
  }

  renderOptionTemplateItems() {
    if (
      !_.has(
        this.state,
        `currentlyEditedOption.${
          this.state.defaultBackendLanguage.key
        }.optionItems`
      )
    ) {
      return null;
    }
    const optionItems = this.state.currentlyEditedOption[
      this.state.selectedLanguage.key
    ].optionItems;

    return (
      <Table responsive size="sm">
        <thead>
          <tr>
            <th>Name</th>
            <th>Included
             <Field
              style={{margin:"8px 0px -2px 6px",width:"25px",height:"25px"}}
               checked={this.checkifAllSelected(optionItems,'included')}
               name="checkAllIncluded"
               component="input"
               type="checkbox"
               onChange={event => this.checkUncheckAllOptions(event,optionItems,'included')}
             />
            </th>
            <th>Active
              <Field
                 checked={this.checkifAllSelected(optionItems,'active')}
                 style={{margin:"8px 0px -2px 6px",width:"25px",height:"25px"}}
                 name="checkAllActive"
                 component="input"
                 type="checkbox"
                 onChange={event => this.checkUncheckAllOptions(event,optionItems,'active')}
               />
            </th>
            <th style={{width:"200px"}}>PHC Sub-Menu
             <input
              name="setSameSubMenu"
              style={{margin:"8px 0px -2px 6px",border:"1px solid #e4e7ea",borderRadius:"0.25rem",display:"inline-block",width:"65px",height:"35px"}}
              disabled={false}
              value={this.checkIfAllOptionsHaveTheSameSubMenu(optionItems)}
              type="input"
              onChange={event => this.setSubMenuForAllOptions(event,optionItems)   }
            />
            </th>
            </tr>
        </thead>
        <tbody>
          {optionItems.map((option, optionIndex) => {
            return (
              <tr key={optionIndex}>
                <td>{option.name}</td>
                <td>
                  <Field
                    name="included"
                    style={{width:"18px",height:"18px"}}
                    checked={option.included||false}
                    component="input"
                    type="checkbox"
                    onChange={event =>
                      this.onOptionLiveTableFieldChange(option.alias, event)
                    }
                  />
                  <Badge color={option.included ? 'success' : 'danger'}>
                    {'  '}
                    {option.included ? 'Inculded' : 'Not Included'}
                  </Badge>
                </td>
                <td>
                  <Field
                    name="active"
                    checked={option.active||false}
                    style={{width:"18px",height:"18px"}}
                    component="input"
                    type="checkbox"
                    onChange={event =>
                      this.onOptionLiveTableFieldChange(option.alias, event)
                    }
                  />
                  <Badge color={option.active ? 'success' : 'danger'}>
                    {'  '}
                    {option.active ? 'Active' : 'Not Active'}
                  </Badge>
                </td>
                <td>
                  <Field
                   name="phcSubMenu"
                   valueFromState={option.phcSubMenu||""}
                   component={renderModalTextInput}
                   onChange={event => this.onOptionLiveTableFieldChange(option.alias, event)}
                    />
                </td>

              </tr>
            );
          })}
        </tbody>
      </Table>
    );
  }

  addOption() {


    const optionAlias = this.state.currentlyEditedOption[
      this.state.defaultBackendLanguage.key
    ].alias
      ? this.state.currentlyEditedOption[this.state.defaultBackendLanguage.key]
          .alias
      : _.snakeCase(
          this.state.currentlyEditedOption[
            this.state.defaultBackendLanguage.key
          ].name
        );

    let item = _.cloneDeep(this.state.item);
    let currentlyEditedOption = _.cloneDeep(this.state.currentlyEditedOption);
    LANGUAGES.map(lang => {
      currentlyEditedOption[lang.key].alias = optionAlias;
      currentlyEditedOption[lang.key].langKey = lang.key;
      if (!item[lang.key].options) {
        item[lang.key].options = [];
      }

      if(currentlyEditedOption[lang.key].quantitySelection && currentlyEditedOption[lang.key].min===currentlyEditedOption[lang.key].max && currentlyEditedOption[lang.key].optionItems.length===1){

         currentlyEditedOption[lang.key].optionItems[0].selectedQuantity=  currentlyEditedOption[lang.key].min;
      }

      this.props.category.templates.map((item)=>{


        if (item.alias === currentlyEditedOption[lang.key].templateOptionSelect )
        {
        currentlyEditedOption[lang.key].dependCat= item.dependCat
          item.ingredients.map((option , index ) =>{

            if (option.alias === currentlyEditedOption[lang.key].optionItems[index].alias)
            {
              currentlyEditedOption[lang.key].optionItems[index].catNotDisplay = option.catNotDisplay
            }

          })

        }

    })
      const options = _.unionBy(
        [currentlyEditedOption[lang.key]],
        item[lang.key].options,
        'alias'
      );

      //item[lang.key].options = options;
      item[lang.key].options =_.sortBy(options,'sortIndex');
    });



    this.setState({ item, currentlyEditedOption: null });
  }

  editOption(optionIndex) {

    const currentlyEditedOption = _.mapKeys(
      _.cloneDeep(
        LANGUAGES.map(lang => {
          return {
            ..._.cloneDeep(this.state.item[lang.key].options[optionIndex])
          };
        })
      ),
      'langKey'
    );

    this.setState({ currentlyEditedOption });
  }

  renderOptionsReadOnly() {
    if (!_.has(this.state, `item.${this.state.selectedLanguage.key}.options`)) {
      return null;
    }

    return (
      <Card>
        <CardHeader className="header-black">Options</CardHeader>
        <CardBody>
          <Table responsive size="sm">
            <thead>
              <tr>
                <th>Name</th>
                <th>Active</th>
                <th>Min</th>
                <th>Max</th>
                <th>Order</th>
                <th>Action</th>
              </tr>
            </thead>
            <tbody>
              {this.state.item[this.state.selectedLanguage.key].options.map(
                (option, optionIndex) => {
                  return (
                    <tr key={optionIndex}>
                      <td>{option.name}</td>
                      <td>
                        {option.active ? (
                          <Badge color="success">Active</Badge>
                        ) : (
                          <Badge color="danger">Inactive</Badge>
                        )}
                      </td>
                      <td>{option.min}</td>
                      <td>{option.max}</td>
                      <td>{_.has(option,'sortIndex')?option.sortIndex:''}</td>
                      <td>
                        <Button
                          color="warning"
                          disabled={false}
                          onClick={() => this.editOption(optionIndex)}>
                          EDIT
                        </Button>
                      </td>
                    </tr>
                  );
                }
              )}
            </tbody>
          </Table>
        </CardBody>
      </Card>
    );
  }

  renderOptions() {
    const optionTemplates = (
      this.props.category.templates || []
    ).map((templ, templIndex) => {
      return (
        <option key={templIndex} name={templ.alias} value={templ.alias}>
          {templ.name}
        </option>
      );
    });

    return (
      <Card>
        <CardHeader className="header-black">Item Options</CardHeader>
        {/* <CardBody>
          <Row>
            {LANGUAGES.map((lang, i) => {
              return (
                <Col key={i} xs="12" md="2">
                  <Field
                    label={`${lang.description} Option Name`}
                    name="name"
                    disabled={false}
                    valueFromState={_.get(
                      this.state.currentlyEditedOption,
                      `${lang.key}.name`,
                      ''
                    )}
                    component={renderModalTextInput}
                    onChange={event =>
                      this.onOptionFieldChange(lang.key, event)
                    }
                  />
                </Col>
              );
            })}
            <Col xs="12" md="1">
              <Field
                label="Minimum"
                name="min"
                disabled={false}
                valueFromState={_.get(
                  this.state.currentlyEditedOption,
                  `${this.state.defaultBackendLanguage.key}.min`,
                  ''
                )}
                component={renderModalNumberInput}
                onChange={event => this.onOptionFieldChange(null, event)}
              />
            </Col>
            <Col xs="12" md="1">
              <Field
                label="Maximum"
                name="max"
                disabled={false}
                valueFromState={_.get(
                  this.state.currentlyEditedOption,
                  `${this.state.defaultBackendLanguage.key}.max`,
                  ''
                )}
                component={renderModalNumberInput}
                onChange={event => this.onOptionFieldChange(null, event)}
              />
            </Col>
            <Col xs="12" md="1">
            <Field
              label="Sort Position"
              name="sortIndex"
              valueFromState={_.get(
                this.state.currentlyEditedOption,
                `${this.state.defaultBackendLanguage.key}.sortIndex`,''
              )}
              component={renderModalNumberInput}
              onChange={event => this.onOptionFieldChange(null, event)}
            />
            </Col>
            <Col xs="12" md="2">
             <div className="form-group" >
                <label>Is Quantity Selection?</label>
                <input
                   name="quantitySelection"
                  className="form-control"
                  type="checkbox"
                  onChange={event => this.onOptionFieldChange(null, event)}
                    checked={_.get(
                      this.state.currentlyEditedOption,
                      `${this.state.defaultBackendLanguage.key}.quantitySelection`,
                      false
                    )}
                    />
              </div>
            </Col>
            <Col xs="12" md="1">
             <div className="form-group" >
                <label>Active</label>
                <input
                   name="active"
                  className="form-control"
                  type="checkbox"
                  onChange={event => this.onOptionFieldChange(null, event)}
                    checked={_.get(
                      this.state.currentlyEditedOption,
                      `${this.state.defaultBackendLanguage.key}.active`,
                      false
                    )}
                    />
              </div>
            </Col>
            <Col xs="12" md="1">
               <label>Template </label>
                   <select
                      name="templateOptionSelect"
                      value={_.get( this.state.currentlyEditedOption,
                           `${this.state.defaultBackendLanguage.key}.templateOptionSelect`,
                           ''
                         )}
                       onChange={event => this.onOptionFieldChange(null, event)}>
                       <option value="" />
                       {optionTemplates}
                   </select>

            </Col>
            <Col xs="12" md="1">
              <label />
              <Button
                className="float-right"
                color="danger"
                disabled={
                  !this.state.currentlyEditedOption ||
                  !this.state.currentlyEditedOption[
                    this.state.defaultBackendLanguage.key
                  ] ||
                  Object.keys(
                    this.state.currentlyEditedOption[
                      this.state.defaultBackendLanguage.key
                    ]
                  ).length < 7
                }
                onClick={this.addOption}>
                SAVE
              </Button>
            </Col>
           {this.state.currentlyEditedOption?this._renderExcludedOptionsForSize():null}
          </Row>

          {this.renderOptionTemplateItems()}
        </CardBody> */}
        {this.renderOptionsReadOnly()}
      </Card>
    );
  }

   renderImages() {
     return (
     <Col xs={12} sm={6} md={6}>
       <Card >
         <CardHeader className="header-black" >Image</CardHeader>
         <CardBody>
           <Row >
               <Col >
                  <div>
                     <Row>
                       <Col >
                         <input
                           className="form-control"
                           type="file"
                           onChange={(e)=>this.fileChangedHandlerImage(e)}
                        />
                       </Col>
                       <Col >
                           {this.state.item.el.image?
                             <img
                                src= {`https://deliverymanstorage.blob.core.windows.net/menuitemsphotos/${((this.state.item || {})[this.state.selectedLanguage.key] || {}).image || ''}`}
                               style={{ width: '60%' }}
                             />
                             :null
                           }

                       </Col>
                       <Col >
                            <Button style={{width:'150px',marginTop:'5px'}} name='deleteImage' onClick={(e)=>this.onFieldChange(null,e)}>Delete Image</Button>
                       </Col>
                     </Row>
                  </div>
               </Col>
           </Row>
         </CardBody>
       </Card>
       </Col>
     );
   }

  AnyForm = () =>{
    return(
      <ModalBody>
      {this.renderItemsBasicInfo()}
      {this.renderSizePrices()}
      {/* {this.renderDiscounts()}
      {this.renderAllergies()} */}
      {/* {this.renderOptions()} */}
      </ModalBody>
    )
  }

  _renderExcludedOptionsForSize = () =>{

   if (this.props.sizes && this.state.item) {

    const hiddenForSizes = _.get(this.state.currentlyEditedOption, `${this.state.defaultBackendLanguage.key}.hiddenForSizes`,[])
    const checkBoxFields =  this.props.sizes.map((size, i) => {

       return (
          <div key={size.alias} style={{margin:"8px"}}>
            <input
             name={size.alias}
             type="checkbox"
             onChange={event =>  this._excludeOptionFromSize(event,size.alias)}
             checked={hiddenForSizes.indexOf(size.alias)>=0?true:false}
              />
            <span style={{marginLeft:"5px"}}>{size.size}</span>
            </div>
     )})

     return ( <Col xs="12">
                <label style={{fontSize:"14px",margin:"8px"}}>Option Not Available For Sizes </label>
                <div style={{display:"flex",marginBottom:"1rem"}}>  {checkBoxFields}</div>
              </Col>)
    }
    else
      return  null
  }

  render() {
   
    return (
      <div>
        
      <Modal
        style={{maxWidth:90+'%'}}
        isOpen={this.props.showMenuItemModal}//{this.props.showMenuItemModal}
        className='dm-modal-full modal-danger'>{}
        <ModalHeader>Item Basic Info</ModalHeader>

        {this.AnyForm()}

        <ModalFooter>
         {/* {this.state.loading?<img src={savingGif} width="50px" height="50px" />:''} */}
          <Button
            color="success"
            disabled={!_.has(this.state.item, `${this.state.selectedLanguage.key}.name`) || !this.props.isAddButtonEnabled}
            onClick={this.updateParentState}>
            Update Item
          </Button>
          <Button color="secondary" onClick={this.resetModal}
            disabled={!this.props.isAddButtonEnabled}>
            Cancel
          </Button>
        </ModalFooter>
      </Modal>
      </div>
    );
  }
}

MenuItemPopup = reduxForm({
  form: 'formname'
})(MenuItemPopup)


export default connect(
  null,
  {}
)(MenuItemPopup);

