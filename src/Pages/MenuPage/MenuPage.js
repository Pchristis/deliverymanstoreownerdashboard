import React from 'react';
import { Link, Redirect } from 'react-router-dom';
import { connect } from 'react-redux';
import { Dropdown } from 'primereact/dropdown';
import {Button} from 'primereact/button';
import {InputSwitch} from 'primereact/inputswitch';
import {Accordion,AccordionTab} from 'primereact/accordion';
import {TabView,TabPanel} from 'primereact/tabview';
import {Calendar} from 'primereact/calendar';
import {OverlayPanel} from 'primereact/overlaypanel';
import {Messages} from 'primereact/messages';

import { menuActions } from '../../actions';
import { menuCategoriesActions } from '../../actions/menuCategories.actions';
import MenuItemPopup from './MenuItemPopup.js'
import {performRequest} from '../../apis/requestWrapper'
import _ from 'lodash';

const LANGUAGES = [
  { key: 'el', description: 'Greek/Ελληνικά' },
  { key: 'en', description: 'English/Αγγλικά' }
];

const MENU_IMAGES_BASE_URL = "https://deliverymanstorage.blob.core.windows.net/menuitemsphotos/";

class MenuPage extends React.Component {
  constructor(props) {
      super(props);

      let selectedStore = null;

      if (localStorage.getItem('deliveryman-store')){
        selectedStore = JSON.parse(localStorage.getItem('deliveryman-store'));
      }

      this.state = {
            store: selectedStore,
            selectedMenu: null,
            selectedMenuItem: null,
            selectedMenuOption: null,
            selectedMenuItemOption: null,
            changeStatus: false,
            showMenuItemModal: false,
            availableDays: {
              "MON": {"from": null, "to": null},
              "TUE": {"from": null, "to": null},
              "WED": {"from": null, "to": null},
              "THU": {"from": null, "to": null},
              "FRI": {"from": null, "to": null},
              "SAT": {"from": null, "to": null},
              "SUN": {"from": null, "to": null},
            },
            editItemPopupData:null,
            isAddButtonEnabled:true
      };


      if (this.state.changeStatus == false){
        if ((this.props.stores) && (this.state.store == null)){
          setTimeout(function() {
            this.setState({
              store: this.props.stores[0]
            });
            localStorage.setItem('deliveryman-store', JSON.stringify(this.props.stores[0]));
          }.bind(this), 500);
        }

        if (this.state.store){
          setTimeout(function() {
            document.getElementById("filterButton").click();
          }.bind(this), 500);

          this.setState({
            changeStatus: true
          });
        }
      }

      this.onStoreChange = this.onStoreChange.bind(this);
      this.changeMenuStatus = this.changeMenuStatus.bind(this);
      this.handleMenuSubmit = this.handleMenuSubmit.bind(this);
      this.clickOnAvailableHours = this.clickOnAvailableHours.bind(this);
      this.handleAvailableHoursSubmit = this.handleAvailableHoursSubmit.bind(this);
      this.updateAvailableDaysState = this.updateAvailableDaysState.bind(this);
      this.validateAvailableDaysState = this.validateAvailableDaysState.bind(this);
      this.showSuccess = this.showSuccess.bind(this);
      this.showError = this.showError.bind(this);
  }

  showSuccess() {
        this.messages.show({severity: 'success', summary: '', detail: 'Available hours have beeen submitted succesfully!'});
  }

  showError(msg) {
        this.messages.show({life: 5000, severity: 'error', summary: '', detail: msg});
  }

  onStoreChange(e) {
      this.setState({
        store: e.value,
        changeStatus: true
      });

      localStorage.setItem('deliveryman-store', JSON.stringify(e.value));
  }

  changeMenuStatus(menuAlias, menuItemAlias, menuOptionAlias, menuItemOptionAlias,e) {
    let storeId = this.state.store.id;
    let state = e.value ? true : false

    const { dispatch } = this.props;

    if (storeId && menuAlias){
      dispatch(menuActions.updateMenuStatus(menuAlias, menuItemAlias, menuOptionAlias, menuItemOptionAlias, storeId, state));
    }
  }


  updateAvailableDaysState(day, fromto, e) {
    let tempAvailableDays = this.state.availableDays;
    tempAvailableDays[day][fromto] = e.value;

    this.setState({
      availableDays: tempAvailableDays
    });

  }

  clickOnAvailableHours(menuDescription, menuAlias, menuItemDescription, menuItemAlias, menuOptionDescription, menuOptionAlias, menuItemOptionDescription, menuItemOptionAlias, availableHours,e) {
    const days = ['MON', 'TUE', 'WED', 'THU', 'FRI', 'SAT', 'SUN'];
    const fromTo = ['from', 'to'];
    let tempAvailableDays = this.state.availableDays;
    let dateTemp = null;
    for (var i = 0; i < days.length; i++) {
      for (var j = 0; j < fromTo.length; j++) {
        if (!(availableHours[days[i]][fromTo[j]] instanceof Date)){
          dateTemp = new Date();
          if (availableHours[days[i]][fromTo[j]]){
            let tempArray = availableHours[days[i]][fromTo[j]].split(":");
            dateTemp.setHours(tempArray[0],tempArray[1],0);
          }else{
            dateTemp = null;
          }
        }else{
          dateTemp = availableHours[days[i]][fromTo[j]];
        }
        tempAvailableDays[days[i]][fromTo[j]] = dateTemp;
      }
    }

    this.setState({
      selectedMenu: {'name': menuDescription, 'alias': menuAlias},
      selectedMenuItem: {'name': menuItemDescription, 'alias': menuItemAlias},
      selectedMenuOption: {'name': menuOptionDescription, 'alias': menuOptionAlias},
      selectedMenuItemOption: {'name': menuItemOptionDescription, 'alias': menuItemOptionAlias},
      availableDays: tempAvailableDays
    });

  }

  validateAvailableDaysState(e){
    let tempAvailableDays = this.state.availableDays;
    let errors = [];
    let errorFlag1 = true;
    let errorFlag2 = true;
    for (var day in tempAvailableDays) {
        if (tempAvailableDays[day]['from'] && tempAvailableDays[day]['to']) {
          if (tempAvailableDays[day]['from'] > tempAvailableDays[day]['to']){
            if (errorFlag1){
              errors.push("'From' time must be before the 'to' time.");
              errorFlag1 = false;
            }
          }
        }else if (!tempAvailableDays[day]['from'] && !tempAvailableDays[day]['to']) {
            delete tempAvailableDays[day]['from'];
            delete tempAvailableDays[day]['to'];
        }else if ((!tempAvailableDays[day]['from'] && tempAvailableDays[day]['to']) || (tempAvailableDays[day]['from'] && !tempAvailableDays[day]['to'])){
          if (errorFlag2){
            errors.push("If 'from' time is set then 'to' time must be also set.");
            errorFlag2 = false;
          }
        }
    }

    this.setState({
      availableDays: tempAvailableDays
    });

    if (errors.length > 0) {
      this.showError(errors.join(", "));
      return false;
    }else{
      return true;
    }

  }

  handleAvailableHoursSubmit(e) {
    if (this.validateAvailableDaysState()){
      let tempAvailableDaysToAction = this.state.availableDays;
      for (var day in tempAvailableDaysToAction) {
          if (tempAvailableDaysToAction[day]['from'] && tempAvailableDaysToAction[day]['to']) {
              tempAvailableDaysToAction[day]['from'] = tempAvailableDaysToAction[day]['from'].getHours() + ':' + (tempAvailableDaysToAction[day]['from'].getMinutes()<10?'0':'') + tempAvailableDaysToAction[day]['from'].getMinutes();
              tempAvailableDaysToAction[day]['to'] = tempAvailableDaysToAction[day]['to'].getHours() + ':' + (tempAvailableDaysToAction[day]['to'].getMinutes()<10?'0':'') + tempAvailableDaysToAction[day]['to'].getMinutes();
          }
      }
      const { dispatch } = this.props;
      dispatch(menuActions.updateMenuAvailableHours(this.state.selectedMenu.alias, this.state.selectedMenuItem.alias, this.state.selectedMenuOption.alias, this.state.selectedMenuItemOption.alias, this.state.store.id, tempAvailableDaysToAction));
      this.showSuccess();
    }
  }

  componentDidMount() {
  }

  handleMenuSubmit(e) {
    e.preventDefault();

    let { store } = this.state;
    const { dispatch } = this.props;

    if (store) {
      dispatch(menuActions.filterMenu(store.id));
      dispatch(menuCategoriesActions.getStoreCategories(store.id)); //TODO Add it to the proper place in code
    }
  }

  renderMenuEditPopup = () => {
    let categoryEN,categoryEL,itemEN,itemEL,item,category;
    //console.log("this.state.editItemPopupData",this.state.editItemPopupData);
    //console.log("this.props.menuCategories",this.props.menuCategories);

    //Check if the data is available and trasform them so they are compatible with the MenuItemPopup component
    if(this.props.menuCategories && this.props.menuCategories.en && this.props.menuCategories.el && this.state.editItemPopupData && this.state.editItemPopupData.categoryAlias){
       categoryEN = this.props.menuCategories.en.filter((category)=>{
        return category.alias == this.state.editItemPopupData.categoryAlias
      })
       categoryEL = this.props.menuCategories.el.filter((category)=>{
        return category.alias == this.state.editItemPopupData.categoryAlias
      })
      categoryEL = categoryEL[0]
      categoryEN = categoryEN[0]

      // console.log("categoryEL",categoryEL)
      // console.log("categoryEN",categoryEN)

      if(!(categoryEN && categoryEL && categoryEN.itemDetails && categoryEL.itemDetails))
        return null;

       itemEN = categoryEN.itemDetails.filter((item)=>{
        return(item.alias == this.state.editItemPopupData.itemAlias)
      })
       itemEL = categoryEL.itemDetails.filter((item)=>{
        return(item.alias == this.state.editItemPopupData.itemAlias)
      })

      itemEN = itemEN[0];
      itemEL = itemEL[0];

       category = {
        el:categoryEL,
        en:categoryEN
      }
       item = {
        el:itemEL,
        en:itemEN
      }
    
      return(
        item && category ?
          <div className="col-md-12 menuPage">
            <MenuItemPopup
              category = {category}
              sizes = {undefined}
              item = {item}
              onMenuItemUpdate = {this.onMenuItemUpdate}
              closeShowMenuItemModal={() =>this.handleMenuItemModal()}
              showMenuItemModal={this.state.showMenuItemModal}//{this.state[`showMenuItemModal${category.alias}`]}
              isAddButtonEnabled={this.state.isAddButtonEnabled}
            />
          </div>
        : null
      )
    }
    else
      return null;
  }

  onMenuItemUpdate = (item,category) => {
    this.setState({isAddButtonEnabled: false});
    let categoryStructure = {
      languages:["el","en"],
      menuCategories:category
    }

    let clonedCategory = _.cloneDeep(categoryStructure);

    clonedCategory.languages.map(lang => {
      let itemDetails = _.unionBy(
        [item[lang]],
        clonedCategory.menuCategories[lang].itemDetails,
        'alias'
      );

      if(item[lang]["itemCode"]){
        item[lang]["alias"] = item[lang]["itemCode"]
        delete item[lang]["itemCode"]
      }
      clonedCategory.menuCategories[lang].itemDetails =_.sortBy(itemDetails,'sortIndex');
    });

      this.setState({ clonedCategory },() => {
      this.onCategoryChange(clonedCategory);
    });
  
  };

  onCategoryChange = category => {
    category.storeId=this.state.store.id;
    category.languages = LANGUAGES.map(lang=> lang.key);
    
    let newCategory = {
      storeId:this.state.store.id,
      languages:LANGUAGES.map(lang=> lang.key),
      en:category.menuCategories.en,
      el:category.menuCategories.el
    }

    performRequest('POST', `createUpdateMenuCategoryByStoreId`,newCategory,true).then(response => {
      //console.log("createUpdateMenuCategoryByStoreId",response);
      this.setState({
        showMenuItemModal: false,
        isAddButtonEnabled: true
      });

      //Update the current state of menu. TODO make it more efficient and update the state via the data above.
      let { store } = this.state;
      const { dispatch } = this.props;
      if (store) {
        dispatch(menuActions.filterMenu(store.id));
        dispatch(menuCategoriesActions.getStoreCategories(store.id));
      }      
    })

    // this.props.createupdateMenucategoryByStoreId(category,(res)=>{
    //   let storeMenuCatsLocal = _.cloneDeep(this.state.storeMenuCats);
    //   LANGUAGES.map(lang => {
    //     if (lang.key===this.state.defaultBackendLanguage.key)
    //       res[this.state.defaultBackendLanguage.key].ts = new Date().getTime();

    //     if (!storeMenuCatsLocal[lang.key]){
    //       storeMenuCatsLocal[lang.key] = [res[lang.key]];
    //     }
    //     else if(storeMenuCatsLocal[lang.key].findIndex(cat => cat.alias === res[lang.key].alias) >= 0){
    //       const catIndex = storeMenuCatsLocal[lang.key].findIndex(cat => cat.alias === res[lang.key].alias);
    //       storeMenuCatsLocal[lang.key][catIndex] = res[lang.key];
    //     }
    //     else{
    //       storeMenuCatsLocal[lang.key].push(res[lang.key]);
    //     }

    //     storeMenuCatsLocal[lang.key] = _.sortBy(storeMenuCatsLocal[lang.key],'sortIndex');
    //   });
    //   this.setState({storeMenuCats:storeMenuCatsLocal})
    // });  
  };

  handleMenuItemModal = (categoryAlias,itemAlias) => {
    //console.log("itemAlias",itemAlias);
    this.setState({
      editItemPopupData: {
        categoryAlias:categoryAlias,
        itemAlias:itemAlias
      },
      showMenuItemModal: !this.state.showMenuItemModal
    });
  }

  render() {
      const { stores } = this.props;
      const { menu } = this.props;
      const { userinfo } = this.props;

      if (userinfo.dashboardViewOnly){
        return <Redirect to={'/'} />
      }
      return (
        <div className="col-md-12 menuPage">
          <div className="row filter-section" id="filter-section">
            <div className="col-md-3">
              {(stores && stores.length) ? (
                <Dropdown value={this.state.store} optionLabel="name" options={stores} onChange={this.onStoreChange} placeholder="Select a Store"/>
              ) : ''}
            </div>
            <div className="col-md-3">
              {this.state.store ? (
                  <button className="btn btn-primary" id="filterButton" onClick={this.handleMenuSubmit}>Filter</button>
              ) : (
                  <button className="btn btn-primary" id="filterButton" onClick={this.handleMenuSubmit} disabled>Filter</button>
              )}
            </div>
          </div>
          {this.state.showMenuItemModal ? this.renderMenuEditPopup() : null}
          <Accordion>
          {menu &&
            menu.map((menu_category, key) => {
              return (
                <AccordionTab header={menu_category.menu_item_description}>
                  <div className="categoryAndItems">
                      <div className="category row">
                        <Button onClick={(event) => {this.clickOnAvailableHours(menu_category.menu_item_description, menu_category.menu_alias, null, null, null, null, null, null, menu_category.availiableHours, event); this.op.toggle(event);}} icon="pi pi-clock" className="availableHoursClock"/>
                        <InputSwitch checked={menu_category.state_status} onChange={(event) => {this.changeMenuStatus(menu_category.menu_alias, '', '', '',event)}}/>
                        <div className="description">{menu_category.menu_item_description}</div>
                      </div>
                      <div className="descriptionTitle row">
                        <p>{menu_category.menu_item_description} Items</p>
                        </div>
                      <div className="categoryItems">
                        {
                          menu_category.category_items.map((category_item, key2) => {
                            return (
                                <div>
                                  <div className="categoryItem row">
                                    <Button onClick={(event) => {this.clickOnAvailableHours(menu_category.menu_item_description, menu_category.menu_alias, category_item.menu_item_description, category_item.menu_item_alias, null, null, null, null, category_item.availiableHours, event); this.op.toggle(event);}} icon="pi pi-clock" className="availableHoursClock"/>
                                    <InputSwitch checked={category_item.state_status} onChange={(event) => {this.changeMenuStatus(menu_category.menu_alias, category_item.menu_item_alias, '', '', event)}}/>
                                    <div className="description">{category_item.menu_item_description} {category_item.menu_item_price?"€":""}{category_item.menu_item_price}</div>
                                    {category_item.menu_item_thumbnail?
                                      <img className="menuThumbnailImage" src={MENU_IMAGES_BASE_URL+category_item.menu_item_thumbnail}></img>
                                      :null
                                    }                                    
                                    <Button className="editButton" icon={"pi pi-pencil"} onClick={()=> this.handleMenuItemModal(menu_category.menu_alias,category_item.menu_item_alias)}></Button>

                                  </div>
                                  <div className="categoryItem row">
                                    {category_item.menu_item_details}
                                  </div>
                                  <div className="categoryItemOptions">
                                    {category_item.options.length > 0 &&
                                      <TabView className="col-md-12">
                                        {
                                          category_item.options.map((option, key3) => {
                                            return (
                                                <TabPanel header={option.name} leftIcon="pi pi-bookmark">
                                                    <div className="row categoryItemOptionItemsOuter">
                                                      {
                                                        option.optionItems.map((option_item, key4) => {
                                                          return (
                                                              <div className="categoryItemOptionItem col-lg-4 col-md-6">
                                                                <Button onClick={(event) => {this.clickOnAvailableHours(menu_category.menu_item_description, menu_category.menu_alias, category_item.menu_item_description, category_item.menu_item_alias, option.name, option.alias, option_item.name, option_item.alias, option_item.availiableHours, event); this.op.toggle(event);}} icon="pi pi-clock" className="availableHoursClock"/>
                                                                <InputSwitch checked={option_item.state_status}  onChange={(event) => {this.changeMenuStatus(menu_category.menu_alias, category_item.menu_item_alias, option.alias, option_item.alias, event)}}/>
                                                                <div className="description">{option_item.name} {option_item.price?"€":""}{option_item.price}</div>
                                                              </div>
                                                            )
                                                        })
                                                      }
                                                    </div>
                                                </TabPanel>
                                              )
                                          })
                                        }
                                      </TabView>
                                    }
                                  </div>
                                </div>
                              )
                          })
                        }
                      </div>
                    </div>
                  </AccordionTab>
              )
            })
          }
          </Accordion>

          <OverlayPanel ref={(el) => this.op = el} showCloseIcon={true} className="customPopup" id="popupMenu">
            {this.state.selectedMenu && this.state.selectedMenuItem && this.state.selectedMenuOption && this.state.selectedMenuItemOption && (
              <div>
                <div className="title">{this.state.selectedMenu.name} {(this.state.selectedMenuItem.name ? ('/ '+this.state.selectedMenuItem.name) : '')} {(this.state.selectedMenuOption.name ? ('/ '+this.state.selectedMenuOption.name) : '')} {(this.state.selectedMenuItemOption.name ? ('/ '+this.state.selectedMenuItemOption.name) : '')}</div>
                <div>
                  <div className="dayDescription">Monday</div>
                  <div className="row datesRow">
                    <div className="col-md-6">
                      <span className="description">From</span>
                      <Calendar value={this.state.availableDays.MON.from} onChange={(e) => this.updateAvailableDaysState('MON', 'from', e)} timeOnly={true} hourFormat="24" showSeconds={false} />
                    </div>
                    <div className="col-md-6">
                      <span className="description">To</span>
                      <Calendar value={this.state.availableDays.MON.to} onChange={(e) => this.updateAvailableDaysState('MON', 'to', e)} timeOnly={true} hourFormat="24" showSeconds={false} />
                    </div>
                  </div>
                  <div className="dayDescription">Tuesday</div>
                  <div className="row datesRow">
                    <div className="col-md-6">
                      <span className="description">From</span>
                      <Calendar value={this.state.availableDays.TUE.from} onChange={(e) => this.updateAvailableDaysState('TUE', 'from', e)} timeOnly={true} hourFormat="24" showSeconds={false} />
                    </div>
                    <div className="col-md-6">
                      <span className="description">To</span>
                      <Calendar value={this.state.availableDays.TUE.to} onChange={(e) => this.updateAvailableDaysState('TUE', 'to', e)} timeOnly={true} hourFormat="24" showSeconds={false} />
                    </div>
                  </div>
                  <div className="dayDescription">Wednesday</div>
                  <div className="row datesRow">
                    <div className="col-md-6">
                      <span className="description">From</span>
                      <Calendar value={this.state.availableDays.WED.from} onChange={(e) => this.updateAvailableDaysState('WED', 'from', e)} timeOnly={true} hourFormat="24" showSeconds={false} />
                    </div>
                    <div className="col-md-6">
                      <span className="description">To</span>
                      <Calendar value={this.state.availableDays.WED.to} onChange={(e) => this.updateAvailableDaysState('WED', 'to', e)} timeOnly={true} hourFormat="24" showSeconds={false} />
                    </div>
                  </div>
                  <div className="dayDescription">Thursday</div>
                  <div className="row datesRow">
                    <div className="col-md-6">
                      <span className="description">From</span>
                      <Calendar value={this.state.availableDays.THU.from} onChange={(e) => this.updateAvailableDaysState('THU', 'from', e)} timeOnly={true} hourFormat="24" showSeconds={false} />
                    </div>
                    <div className="col-md-6">
                      <span className="description">To</span>
                      <Calendar value={this.state.availableDays.THU.to} onChange={(e) => this.updateAvailableDaysState('THU', 'to', e)} timeOnly={true} hourFormat="24" showSeconds={false} />
                    </div>
                  </div>
                  <div className="dayDescription">Friday</div>
                  <div className="row datesRow">
                    <div className="col-md-6">
                      <span className="description">From</span>
                      <Calendar value={this.state.availableDays.FRI.from} onChange={(e) => this.updateAvailableDaysState('FRI', 'from', e)} timeOnly={true} hourFormat="24" showSeconds={false} />
                    </div>
                    <div className="col-md-6">
                      <span className="description">To</span>
                      <Calendar value={this.state.availableDays.FRI.to} onChange={(e) => this.updateAvailableDaysState('FRI', 'to', e)} timeOnly={true} hourFormat="24" showSeconds={false} />
                    </div>
                  </div>
                  <div className="dayDescription">Saturday</div>
                  <div className="row datesRow">
                    <div className="col-md-6">
                      <span className="description">From</span>
                      <Calendar value={this.state.availableDays.SAT.from} onChange={(e) => this.updateAvailableDaysState('SAT', 'from', e)} timeOnly={true} hourFormat="24" showSeconds={false} />
                    </div>
                    <div className="col-md-6">
                      <span className="description">To</span>
                      <Calendar value={this.state.availableDays.SAT.to} onChange={(e) => this.updateAvailableDaysState('SAT', 'to', e)} timeOnly={true} hourFormat="24" showSeconds={false} />
                    </div>
                  </div>
                  <div className="dayDescription">Sunday</div>
                  <div className="row datesRow">
                    <div className="col-md-6">
                      <span className="description">From</span>
                      <Calendar value={this.state.availableDays.SUN.from} onChange={(e) => this.updateAvailableDaysState('SUN', 'from', e)} timeOnly={true} hourFormat="24" showSeconds={false} />
                    </div>
                    <div className="col-md-6">
                      <span className="description">To</span>
                      <Calendar value={this.state.availableDays.SUN.to} onChange={(e) => this.updateAvailableDaysState('SUN', 'to', e)} timeOnly={true} hourFormat="24" showSeconds={false} />
                    </div>
                  </div>
                  <div className="align-left paddingTop">
                    <Messages ref={(el) => this.messages = el} />
                    <button className="btn btn-block btn-primary" onClick={this.handleAvailableHoursSubmit}>Save</button>
                  </div>
                </div>
              </div>
            )}
          </OverlayPanel>
        </div>
      );
  }
}

function mapStateToProps(state) {
    const { authentication, stores, menu, userinfo, menuCategories } = state;
    return {
        user: authentication, stores, menu, userinfo, menuCategories
    };
}

const connectedMenuPage = connect(mapStateToProps, )(MenuPage);
export { connectedMenuPage as MenuPage };
