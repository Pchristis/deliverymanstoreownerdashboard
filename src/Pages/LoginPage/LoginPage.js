import React from 'react';
import { connect } from 'react-redux';
import { Link } from 'react-router-dom';

import { userActions } from '../../actions';
import { setCookie, deleteCookie } from '../../utils/cookies';

class LoginPage extends React.Component {

    constructor(props) {
        super(props);

        // reset login status
        this.props.dispatch(userActions.logout());

        this.state = {
            username: '',
            password: '',
            rememberme: false,
            error: false,
            submitted: false,
            submittedSocial: false
        };

        this.handleChange = this.handleChange.bind(this);
        this.handleSubmit = this.handleSubmit.bind(this);
        this.handleGoogleClick = this.handleGoogleClick.bind(this);
        this.handleFacebookClick = this.handleFacebookClick.bind(this);

    }

    handleChange(e) {
        const { name, value } = e.target;
        this.setState({ [name]: value });
    }

    handleCheckboxChange = event =>
        this.setState({ rememberme: event.target.checked })

    handleSubmit(e) {
        e.preventDefault();
        deleteCookie('rememberme');
        this.setState({ submitted: true });
        const { username, password, rememberme } = this.state;

        if (rememberme){
          setCookie('rememberme', 'true', 250);
        }

        const { dispatch } = this.props;
        if (username && password) {
          dispatch(userActions.login(username, password));
        }
    }

    handleGoogleClick(e) {
        e.preventDefault();
        this.setState({ submittedSocial: true });
        const { dispatch } = this.props;
        try {
          dispatch(userActions.socialMediaLogin('google-oauth2'));
        }
        catch(err) {
          this.setState({ error: true });
        }
    }

    handleFacebookClick(e) {
        e.preventDefault();
        this.setState({ submittedSocial: true });
        const { dispatch } = this.props;
        try {
          dispatch(userActions.socialMediaLogin('facebook'));
        }
        catch(err) {
          this.setState({ error: true });
        }
    }


    render() {
        const { user } = this.props;
        const { username, password, submitted, submittedSocial, error } = this.state;
        return (
          <div  className="container" id="login-page">
            <div className="flex-block">
              <div className="col-md-6 login-flex-block">
                <div className="login-box">
                    <img src="/assets/images/logo.svg" alt="DeliveryMan" id="login-logo"/>
                    <p className="login-description">Login to access the Dashboard</p>
                    <form name="form" onSubmit={this.handleSubmit}>
                        <div className={'form-group' + (submitted && (!username || error) ? ' has-error' : '')}>
                            <input type="text" className="form-control" name="username" placeholder="Username" value={username} onChange={this.handleChange} />
                            {submitted && !username && !submittedSocial &&
                                <div className="help-block">Username is required</div>
                            }
                        </div>
                        <div className={'form-group' + (submitted && (!password || error) ? ' has-error' : '')}>
                            <input type="password" className="form-control" name="password" placeholder="Password" value={password} onChange={this.handleChange} />
                            {submitted && !password && !submittedSocial &&
                                <div className="help-block">Password is required</div>
                            }
                        </div>
                        <div className="form-group">
                            {submitted && user.error && !submittedSocial &&
                              <div className="help-block"><p>{user.description}</p></div>
                            }
                            <button className="btn btn-primary btn-block">Sign In</button>
                        </div>
                        <div className="login-help-inputs">
                          <div>
                            <label><input type="checkbox" name="rememberme" value="true" onChange={this.handleCheckboxChange}/> <span className="rememberme">Remember me</span></label>
                          </div>
                          <div>
                            <div className="login-link">
                              <Link to="/forgotpassword" >Forgot Password</Link>
                            </div>
                          </div>
                        </div>
                    </form>

                    <div className="social-login">
                    <p className="login-description"></p>
                      <button
                          type="button"
                          id="btn-google"
                          className="btn google-btn social-btn"
                          onClick={this.handleGoogleClick}>
                         <span><i className="fab fa-google"></i> Sign in with Google</span>
                      </button>
                      <button
                          type="button"
                          id="btn-facebook"
                          className="btn facebook-btn social-btn"
                          onClick={this.handleFacebookClick}>
                         <span><i className="fab fa-facebook-f"></i> Sign in with Facebook</span>
                      </button>
                    </div>
                </div>
              </div>
              <div className="col-md-6 login-flex-block" id="login-motor"></div>
              <div className="col-md-12 bottom-circles"></div>
            </div>
          </div>
        );
    }
}

function mapStateToProps(state) {
    const { user } = state.authentication;
    return {
        user
    };
}

const connectedLoginPage = connect(mapStateToProps)(LoginPage);
export { connectedLoginPage as LoginPage };
