import React from 'react';
import { Link } from 'react-router-dom';
import { connect } from 'react-redux';
import { Dropdown } from 'primereact/dropdown';
import { Button } from 'primereact/button';
import {Calendar} from 'primereact/calendar';
import { MultiSelect } from 'primereact/multiselect';
import moment from 'moment';
import Highcharts from 'highcharts';
import HighchartsReact from "highcharts-react-official";

import { userActions } from '../../actions';
import { storesActions } from '../../actions';
import { overviewActions } from '../../actions';

class OverviewPage extends React.Component {

  constructor(props) {
      super(props);

      let today = new Date();
      let month = today.getMonth();
      let year = today.getFullYear();
      let prevMonth = (month === 0) ? 11 : month - 1;
      let prevYear = (prevMonth === 11) ? year - 1 : year;
      let minDate = new Date();
      minDate.setMonth(prevMonth);
      minDate.setFullYear(prevYear);
      let maxDate = new Date();

      if (localStorage.getItem('deliveryman-dateRange')){
        let dateRange = JSON.parse(localStorage.getItem('deliveryman-dateRange'));
        minDate = new Date(dateRange[0]);
        maxDate = new Date(dateRange[1]);
      }

      let selectedStore = null;

      if (localStorage.getItem('deliveryman-store')){
        selectedStore = JSON.parse(localStorage.getItem('deliveryman-store'));

        const { dispatch } = this.props;
        dispatch(storesActions.filterCategories(selectedStore.id));
      }

      this.state = {
            store: selectedStore,
            startDateTS: null,
            endDateTS: null,
            categories: null,
            categoriesIds: null,
            changeStatus: false,
            dateRange: [minDate, maxDate]
        };

        this.onStoreChange = this.onStoreChange.bind(this);
        this.onCategoryChange = this.onCategoryChange.bind(this);
        this.onDateRangeChange = this.onDateRangeChange.bind(this);
        this.handleOverviewSubmit = this.handleOverviewSubmit.bind(this);
  }

  componentDidMount(){
    const { dispatch } = this.props;
    dispatch(storesActions.getStores());

    this.setState({
      startDateTS: Date.parse(this.state.dateRange[0]),
      endDateTS: Date.parse(this.state.dateRange[1])
    });

  }

  componentDidUpdate(){
    if (this.state.changeStatus == false){

      if (this.state.store == null){
        try {
          const { dispatch } = this.props;

          if (this.props.stores){
            setTimeout(function() {
              this.setState({
                store: this.props.stores[0]
              });
              localStorage.setItem('deliveryman-store', JSON.stringify(this.props.stores[0]));
              dispatch(storesActions.filterCategories(this.props.stores[0]['id']));

              setTimeout(function() {
                  this.setState({
                    categories: this.props.categories
                  });

                  const categoriesIds = [];
                  this.props.categories.map((category, key) =>
                      categoriesIds.push(category.category_id)
                  );
                  this.setState({categoriesIds: categoriesIds});

                  setTimeout(function() {
                    document.getElementById("filterButton").click();
                  }.bind(this), 500);

              }.bind(this), 500);
            }.bind(this), 500);
          }

        } catch (e) {

        }
      }else if (this.state.categories == null){
        setTimeout(function() {
          this.setState({
            categories: this.props.categories
          });

          const categoriesIds = [];
          this.props.categories.map((category, key) =>
              categoriesIds.push(category.category_id)
          );
          this.setState({categoriesIds: categoriesIds});

          setTimeout(function() {
            document.getElementById("filterButton").click();
          }.bind(this), 500);
        }.bind(this), 500);
      }
      this.setState({
        changeStatus: true
      });
    }
  }

  onDateRangeChange(e) {
    this.setState({
      dateRange: e.value,
      startDateTS: Date.parse(e.value[0]),
      endDateTS: Date.parse(e.value[1]),
      changeStatus: true
    });
    localStorage.setItem('deliveryman-dateRange', JSON.stringify(e.value));
  }

  onStoreChange(e) {
        const { dispatch } = this.props;
        this.setState({
          store: e.value,
          categories: null,
          categoriesIds: null,
          changeStatus: false
        });

        localStorage.setItem('deliveryman-store', JSON.stringify(e.value));
        dispatch(storesActions.filterCategories(e.value.id));
  }

  onCategoryChange(e) {
        this.setState({categories: e.value});
        const categoriesIds = [];
        e.value.map((category, key) =>
            categoriesIds.push(category.category_id)
        );
        this.setState({
          categoriesIds: categoriesIds,
          changeStatus: true
        });
  }

  handleOverviewSubmit(e) {
    e.preventDefault();

    let { startDateTS, endDateTS, categoriesIds, categories, store } = this.state;
    const { dispatch } = this.props;

    if (startDateTS && endDateTS && categoriesIds && categories && store) {
      if (categoriesIds.length === this.props.categories.length){
        categoriesIds = [];
      }

      dispatch(overviewActions.filterOverview(startDateTS, endDateTS, categoriesIds, store.id));
    }

  }

  render() {
      const { userinfo } = this.props;
      const { stores } = this.props;
      const { categories } = this.props;
      const { overview } = this.props;

      let months = ['January','February','March','April','May','June','July','August','September','October','November','December'];
      let monthFrom = new Date(this.state.startDateTS).getMonth();
      monthFrom = months[monthFrom];
      let dayFrom = new Date(this.state.startDateTS).getDate();
      let yearFrom = new Date(this.state.startDateTS).getFullYear();
      let monthTo = new Date(this.state.endDateTS).getMonth();
      monthTo = months[monthTo];
      let dayTo = new Date(this.state.endDateTS).getDate();
      let yearTo = new Date(this.state.endDateTS).getFullYear();

      let dateRange = monthFrom + ' ' + dayFrom + ', ' + yearFrom + ' - ' + monthTo + ' ' + dayTo + ', ' + yearTo;

      let boxRadient = ['box-radient-blue','box-radient-yellow','box-radient-green','box-radient-red','box-radient-blue','box-radient-yellow','box-radient-green','box-radient-red'];

      const lineCategories = [];
      const lineSeries = [];
      const pieSeries = {};
      const pieSeriesData = [];

      let counter = 0;
      if ('SalesPerShopMenuCategories' in overview){
        Object.keys(overview['SalesPerShopMenuCategories']).forEach(function(key) {
          let lineDataObject = {};
          let pieDataObject = {};

          let lineDataObjectData = [];
          let sumOfCategory = 0;

          overview['SalesPerShopMenuCategories'][key].map((recordsDetails, key2) => {
            if (counter === 0){
                lineCategories.push(recordsDetails.date);
            }
            sumOfCategory = sumOfCategory + recordsDetails.sales;
            lineDataObjectData.push(recordsDetails.sales);
          })
          lineDataObject['data'] = lineDataObjectData;
          lineDataObject['name'] = key;

          pieDataObject['y'] = sumOfCategory;
          pieDataObject['name'] = key;

          lineSeries.push(lineDataObject);
          pieSeriesData.push(pieDataObject);
          counter = counter + 1;
        });
        pieSeries['name'] = 'Sales';
        pieSeries['showInLegend'] = 'true';
        pieSeries['data'] = pieSeriesData;
      }


      const lineoptions = {
          chart: {
            type: 'line'
          },
          title: {
              text: 'Sales Per Category'
          },
          subtitle: {
              text: dateRange
          },
          xAxis: {
              categories: lineCategories,
              labels: {
                  rotation: 45
              }
          },
          yAxis: [{
             lineWidth: 1,
             title: {
                 text: 'Sales (€)'
             }
          }],
          credits: {
              enabled: false
          },
          series: lineSeries
        }


        const pieoptions = {
            chart: {
               plotBackgroundColor: null,
               plotBorderWidth: null,
               plotShadow: false,
               type: 'pie'
            },
            title: {
                text: 'Sales Percentage'
            },
            subtitle: {
                text: dateRange
            },
            xAxis: {
               categories: lineCategories
           },
            tooltip: {
                pointFormat: '{series.name}: <b>{point.percentage:.1f}%</b>'
            },
            plotOptions: {
                pie: {
                    allowPointSelect: true,
                    cursor: 'pointer',
                    dataLabels: {
                        enabled: false,
                        format: '<b>{point.name}</b>: {point.percentage:.1f} %',
                        connectorColor: 'silver'
                    }
                }
            },
            credits: {
                enabled: false
            },
            series: [pieSeries]
          }

      return (
        <div className="col-md-12">
          <div className="row filter-section">
            <div className="col-md-3">
              <Calendar value={this.state.dateRange} onChange={this.onDateRangeChange} selectionMode="range" readonlyInput={true} dateFormat="dd/mm/yy" showIcon="true" icon="pi pi-calendar" />
            </div>
            <div className="col-md-3">
            {(stores && stores.length) ? (
              <Dropdown value={(this.state.store ? this.state.store : stores[0])} optionLabel="name" options={stores} onChange={this.onStoreChange} placeholder="Select a Store"/>
            ) : ''}
            </div>
            <div className="col-md-3">
                {categories.length ? (
                    <MultiSelect value={this.state.categories} optionLabel="category_name" options={categories} onChange={this.onCategoryChange} placeholder="Select a Category"/>
                ) : (
                    <MultiSelect value={this.state.categories} optionLabel="category_name" options={categories} onChange={this.onCategoryChange} placeholder="Select a Category" disabled />
                )}
            </div>
            <div className="col-md-3">
                {(categories.length && this.state.categories) ? (
                    <button className="btn btn-primary" id="filterButton" onClick={this.handleOverviewSubmit}>Filter</button>
                ) : (
                    <button className="btn btn-primary" id="filterButton" onClick={this.handleOverviewSubmit} disabled>Filter</button>
                )}
            </div>
          </div>

          {'Sales' in overview &&
            <div>
              <div className="overview-section">
                <div className="row" id="overviewOverallSales">
                  <div className="col-md-2">
                    <div className="description">Overall sales</div>
                    <div className="price">€{(overview['Sales']['overall_sales'])?(overview['Sales']['overall_sales'].toFixed(2)):'0.00'}</div>
                  </div>
                  <div className="col-md-2">
                    <div className="description">Net sales</div>
                    <div className="price">€{(overview['Sales']['net_sales'])?(overview['Sales']['net_sales'].toFixed(2)):'0.00'}</div>
                  </div>
                  <div className="col-md-2">
                    <div className="description">Commission paid</div>
                    <div className="price">€{(overview['Sales']['commission_paid'])?(overview['Sales']['commission_paid'].toFixed(2)):'0.00'}</div>
                  </div>
                  <div className="col-md-2">
                    <div className="description">Total orders</div>
                    <div className="price">{(overview['Sales']['total_orders'])?(overview['Sales']['total_orders']):'0.00'}</div>
                  </div>
                  <div className="col-md-2">
                    <div className="description">Average price/order</div>
                    <div className="price">€{(overview['Sales']['average_price_per_order'])?(overview['Sales']['average_price_per_order'].toFixed(2)):'0.00'}</div>
                  </div>
                </div>
              </div>
              <div id="overviewDates">{dateRange}</div>
            </div>
          }
          {'PaymentMethods' in overview &&
            <div>
              <div className="row" id="overviewPaymentMethods">
                {overview['PaymentMethods'].map((method, key) => {
                    return (
                      <div className="col-md-3">
                        <div className={"box-radient " + boxRadient[key]}>
                          <div className="payment-method">{method.name}</div>
                          <div className="description">Received</div>
                          <div className="price">€{method.received.toFixed(2)}</div>
                          <div className="description">Commission</div>
                          <div className="price">€{method.commision.toFixed(2)}</div>
                        </div>
                      </div>
                    )
                  }
                )}

              </div>
            </div>
          }

          {lineSeries.length ? (
          <div className="row">
            <div className="col-md-8">
              <HighchartsReact highcharts={Highcharts} options={lineoptions} />
            </div>
            <div className="col-md-3">
              <HighchartsReact highcharts={Highcharts} options={pieoptions} />
            </div>
          </div>
        ) : (<p className="noResultsMsg">No results found!</p>)}
        </div>
      );
  }
}

function mapStateToProps(state) {
    const { authentication, userinfo, stores, categories, overview } = state;
    return {
        user: authentication, userinfo, stores, categories, overview
    };
}

const connectedOverviewPage = connect(mapStateToProps)(OverviewPage);
export { connectedOverviewPage as OverviewPage };
