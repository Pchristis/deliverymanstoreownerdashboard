import React from 'react';
import { Link, Redirect } from 'react-router-dom';
import { connect } from 'react-redux';
import {Button} from 'primereact/button';

import {InputSwitch} from 'primereact/inputswitch';
import {Spinner} from 'primereact/spinner';
import {Messages} from 'primereact/messages';
import {OverlayPanel} from 'primereact/overlaypanel';

import { userActions } from '../../actions';
import { restaurantcontrolActions } from '../../actions';


class RestaurantControlPage extends React.Component {

  constructor(props) {
      super(props);

      this.state = {
            flag: true,
            restaurants_data: null,
            selectedRestaurant: null,
            message: null
      };

      const { dispatch } = this.props;
      dispatch(restaurantcontrolActions.getRestaurantControl());

      this.changeRestaurantControlValue = this.changeRestaurantControlValue.bind(this);
      this.changeZoneStatus = this.changeZoneStatus.bind(this);
      this.handleRestaurantControlSubmit = this.handleRestaurantControlSubmit.bind(this);
      this.handleZoneSubmit = this.handleZoneSubmit.bind(this);
      this.showSuccess = this.showSuccess.bind(this);
      this.clickOnCloseZipAreas = this.clickOnCloseZipAreas.bind(this);

  }

  componentWillReceiveProps(nextProps) {
    if (nextProps.restaurantcontrol !== this.state.restaurants_data) {
      this.setState({
        restaurants_data: nextProps.restaurantcontrol
      });
    }
  }

  showSuccess() {
        let previousMsgs = document.getElementsByClassName('p-messages');
        for (let i = 0; i < previousMsgs.length; i++)
        {
            previousMsgs[i].style.display = "none";
        }
        this.messages.show({severity: 'success', summary: '', detail: 'Restaurant control data has been succesfully updated!'});
  }

  clickOnCloseZipAreas(restaurantData,e) {
    this.setState({
      selectedRestaurant: restaurantData,
    });

  }


  changeRestaurantControlValue(control,restaurantID,e) {
    let restaurantsData = this.state.restaurants_data;

    for (var i = 0; i < restaurantsData.length; i++){
      if (restaurantsData[i].restaurant_id == restaurantID){
        if (e.value > 1){
          restaurantsData[i][control] = e.value;
        }else{
          restaurantsData[i][control] = e.value ? 1 : 0;
        }
      }
    }
    this.setState({
      restaurants_data: restaurantsData
    });
  }

  changeZoneStatus(flag,restaurantID, zoneName,postName,e) {
    let restaurantsData = this.state.restaurants_data;

    for (var i = 0; i < restaurantsData.length; i++){
      if (restaurantsData[i].restaurant_id == restaurantID){
        for (var j = 0; j < restaurantsData[i]['storeZonesStatus'].length; j++){
          if (flag == 'zone' && postName == null){
            if (restaurantsData[i]['storeZonesStatus'][j]['zone'] == zoneName){
              restaurantsData[i]['storeZonesStatus'][j]['zoneIsClosed'] = !e.value;
              if (!e.value){
                for (var k = 0; k < restaurantsData[i]['storeZonesStatus'][j]['post_codes'].length; k++){
                  restaurantsData[i]['storeZonesStatus'][j]['post_codes'][k]['isClosed'] = true;
                }
              }else{
                for (var k = 0; k < restaurantsData[i]['storeZonesStatus'][j]['post_codes'].length; k++){
                  restaurantsData[i]['storeZonesStatus'][j]['post_codes'][k]['isClosed'] = false;
                }
              }
            }
          }else if (flag == 'postCode' && postName != null){
            if (restaurantsData[i]['storeZonesStatus'][j]['zone'] == zoneName){
              for (var k = 0; k < restaurantsData[i]['storeZonesStatus'][j]['post_codes'].length; k++){
                if (restaurantsData[i]['storeZonesStatus'][j]['post_codes'][k]['postCode'] == postName){
                  restaurantsData[i]['storeZonesStatus'][j]['post_codes'][k]['isClosed'] = !e.value;
                }
              }
            }
          }
        }
      }
    }
    this.setState({
      restaurants_data: restaurantsData
    });
  }

  handleRestaurantControlSubmit(e) {
    e.preventDefault();

    let { restaurants_data } = this.state;
    const { dispatch } = this.props;

    dispatch(restaurantcontrolActions.updateRestaurantControl(restaurants_data));

    this.showSuccess();

  }

  handleZoneSubmit(selectedRestaurantId, e) {
    e.preventDefault();

    let { restaurants_data } = this.state;

    let restaurantZones = {};
    restaurantZones['restaurant_id'] = selectedRestaurantId;

    for (var i = 0; i < restaurants_data.length; i++){
      if (restaurants_data[i]['restaurant_id'] == selectedRestaurantId){
        restaurantZones['storeZonesStatus'] = restaurants_data[i]['storeZonesStatus'];
      }
    }

    for (var j = 0; j < restaurantZones['storeZonesStatus'].length; j++){
      for (var k = 0; k < restaurantZones['storeZonesStatus'][j]['post_codes'].length; k++){
        if (restaurantZones['storeZonesStatus'][j]['post_codes'][k]['isClosed'] == false){
          delete restaurantZones['storeZonesStatus'][j]['post_codes'][k]['isClosed'];
        }
      }
    }

    const { dispatch } = this.props;

    dispatch(restaurantcontrolActions.updateRestaurantZones(restaurantZones));

  }

  render() {

      const { restaurantcontrol } = this.props;
      const { selectedRestaurant } = this.state;

      const { userinfo } = this.props;

      if (userinfo.dashboardViewOnly){
        return <Redirect to={'/'} />
      }

      let showWarning = {};

      restaurantcontrol.map((restaurant_data, key) => {
        return (
          restaurant_data.storeZonesStatus.map((zoneDetails, keyZone) => {
              if (zoneDetails.zoneIsClosed == true){
                return (
                  showWarning[restaurant_data.restaurant_id] = 'Warning: You have closed some areas!'
                )
              }else{
                if (showWarning[restaurant_data.restaurant_id] != 'Warning: You have closed some areas!'){
                  return (
                    showWarning[restaurant_data.restaurant_id] = ''
                  )
                }
              }
          })
        )
      })

      restaurantcontrol.map((restaurant_data, key) => {
        return (
          restaurant_data.storeZonesStatus.map((zoneDetails, keyZone) => {
              zoneDetails.post_codes.map((postCode, keyCode) => {
                  if (('isClosed' in postCode) && (postCode.isClosed == true)){
                    return (
                      showWarning[restaurant_data.restaurant_id] = 'Warning: You have closed some areas!'
                    )
                  }else{
                    if (showWarning[restaurant_data.restaurant_id] != 'Warning: You have closed some areas!'){
                      return (
                        showWarning[restaurant_data.restaurant_id] = ''
                      )
                    }
                  }
              })
          })
        )
      })

      return (

          <div className="col-md-12 restaurantControlOuterBox">
            {restaurantcontrol &&
              restaurantcontrol.map((restaurant_data, key) => {
                  return (
                    <div className="row restaurantControlInnerBox">
                      <div className="col-md-3">
                        <div className="row">
                          <i className="fa fa-store"></i><span className="restaurantName">{restaurant_data.restaurant_description}</span>
                        </div>
                        <div className="row">
                          <div className="col-md-12">
                            <span className="restaurantDescription">Open / closed</span><InputSwitch checked={restaurant_data.open_status} onChange={(event) => {this.changeRestaurantControlValue('open_status',restaurant_data.restaurant_id, event)}}/>
                          </div>
                        </div>
                      </div>
                      <div className="col-md-5">
                        <div className="row restaurantControlInnerBoxRow">
                          <div className="col-md-4">
                            <span className="restaurantDescription">Take Away</span><InputSwitch checked={restaurant_data.takeaway_status} onChange={(event) => {this.changeRestaurantControlValue('takeaway_status',restaurant_data.restaurant_id, event)}}/>
                          </div>
                          <div className="col-md-4">
                            <span className="restaurantDescription">Expected Time (mins)</span>
                          </div>
                          <div className="col-md-4 restaurantSpinner restaurantSpinnerFirst">
                            <Spinner value={restaurant_data.avgTakeAwayTime} step={5} min={5} max={90} onChange={(event) => {this.changeRestaurantControlValue('avgTakeAwayTime',restaurant_data.restaurant_id, event)}} disabled={!restaurant_data.takeaway_status}/>
                          </div>
                        </div>
                      </div>
                      <div className="col-md-4">
                        <div className="row restaurantControlInnerBoxRow">
                          <div className="col-md-8">
                            <div className="row">
                              <div className="col-md-6 deliveryColumn">
                                <span className="restaurantDescription">Delivery</span><InputSwitch checked={restaurant_data.delivery_status}  onChange={(event) => {this.changeRestaurantControlValue('delivery_status',restaurant_data.restaurant_id, event)}}/>
                              </div>
                              <div className="col-md-6">
                                <span className="restaurantDescription">Expected Time (mins)</span>
                              </div>
                            </div>
                              {restaurant_data.delivery_status == 1 ? (
                                <div className="row">
                                  <Button onClick={(event) => {this.clickOnCloseZipAreas(restaurant_data, event); this.op.toggle(event);}} label="Close specific areas"/>
                                  <p className="warningMessageZip">{showWarning[restaurant_data.restaurant_id]}</p>
                                </div>
                              ) : (
                                <div className="row">
                                  <Button onClick={(event) => {this.clickOnCloseZipAreas(restaurant_data, event); this.op.toggle(event);}} label="Close specific areas" disabled/>
                                </div>
                              )}
                          </div>
                          <div className="col-md-4 restaurantSpinner spinnerLast">
                            <Spinner value={restaurant_data.minutes} step={5} min={5} max={90} onChange={(event) => {this.changeRestaurantControlValue('minutes',restaurant_data.restaurant_id, event)}} disabled={!restaurant_data.delivery_status}/>
                          </div>
                        </div>
                      </div>
                      <OverlayPanel ref={(el) => this.op = el} showCloseIcon={true} className="customPopup" id="popupRestaurantControl">
                        <div>
                        {selectedRestaurant &&
                          <p className="restaurantName">{selectedRestaurant.restaurant_description}</p>
                        }
                        {selectedRestaurant &&
                            selectedRestaurant.storeZonesStatus.map((zoneDetails, keyZone) => {
                              return (
                                <div>
                                  <div className="zoneArea"><span className="zipDescription zoneDescription">{zoneDetails.zone}</span><InputSwitch checked={!zoneDetails.zoneIsClosed} onChange={(event) => {this.changeZoneStatus('zone',selectedRestaurant.restaurant_id, zoneDetails.zone, null,event)}}/></div>
                                  <div className="row postCodeDivRow">
                                  {
                                    zoneDetails.post_codes.map((postCode, keyCode) => {
                                      return (
                                          <div className="col-md-3 postCodeDiv"><span className="zipDescription">{postCode.postCode}</span><InputSwitch checked={!postCode.isClosed} onChange={(event) => {this.changeZoneStatus('postCode',selectedRestaurant.restaurant_id, zoneDetails.zone, postCode.postCode, event)}}/></div>
                                      )
                                    })
                                  }
                                  </div>
                                </div>
                              )
                            })
                          }
                          {selectedRestaurant &&
                            <div className="buttonOverlayDiv">
                              <Button onClick={(event) => {this.handleZoneSubmit(selectedRestaurant.restaurant_id, event);}} label="Save and close"/>
                            </div>
                          }
                        </div>
                      </OverlayPanel>

                    </div>

                  )
                }
              )
            }
            {restaurantcontrol &&
              <div className="col-md-12 text-center">
                <Messages ref={(el) => this.messages = el} />
                <button className="btn btn-primary" onClick={this.handleRestaurantControlSubmit}>Save</button>
              </div>
            }
          </div>
      );
  }
}

function mapStateToProps(state) {
    const { authentication, restaurantcontrol, userinfo } = state;
    return {
        user: authentication, restaurantcontrol, userinfo
    };
}

const connectedRestaurantControlPage = connect(mapStateToProps)(RestaurantControlPage);
export { connectedRestaurantControlPage as RestaurantControlPage };
