import React from 'react';
import { Link } from 'react-router-dom';
import { connect } from 'react-redux';
import {Messages} from 'primereact/messages';

import { userActions } from '../../actions';
import { apiConstants } from '../../constants';
import { getCookie } from '../../utils/cookies';


class SettingsPage extends React.Component {

  constructor(props) {
      super(props);

      this.state = {
          firstname: '',
          lastname: '',
          newPassword: '',
          confirmedPassword: '',
          error: false,
          passwordError: false,
          passwordSubmitted: false,
          nameSurnameSubmitted: false,
          user: null,
          message: false
      };

      const { dispatch } = this.props;
      dispatch(userActions.getUserInfo());
      dispatch(userActions.getUserAuth());

      this.handleNameSurnameChange = this.handleNameSurnameChange.bind(this);
      this.handleNameSurnameSubmit = this.handleNameSurnameSubmit.bind(this);

      this.handlePasswordChange = this.handlePasswordChange.bind(this);
      this.handleConfirmedPasswordChange = this.handleConfirmedPasswordChange.bind(this);
      this.handlePasswordSubmit = this.handlePasswordSubmit.bind(this);
      this.showSuccess = this.showSuccess.bind(this);
  }

  handleNameSurnameChange(e) {
      const { name, value } = e.target;
      this.setState({ [name]: value });
  }

  handlePasswordChange(e) {
      const { name, value } = e.target;
      this.setState({ [name]: value });
  }

  handleConfirmedPasswordChange(e) {
      const { name, value } = e.target;
      this.setState({ [name]: value });
      if ((value !== this.state.newPassword)){
        this.setState({ passwordError: true });
      }else{
        this.setState({ passwordError: false });
      }
  }


  handleNameSurnameSubmit(e) {
    e.preventDefault();

    const { firstname, lastname } = this.state;
    const { dispatch } = this.props;

    if (firstname && lastname) {
      try {
        dispatch(userActions.updateUserInfo(firstname, lastname));
        dispatch(userActions.getUserInfo());
        this.setState({
          nameSurnameSubmitted: true,
          message: true });
        this.showSuccess('nameSurname');
      }catch(err) {
      }
    }

  }

  handlePasswordSubmit(e) {
    e.preventDefault();

    const { newPassword } = this.state;
    const { dispatch } = this.props;
    try {
      dispatch(userActions.changeUserPassword(this.props.user.user.sub, newPassword));
      this.setState({
        passwordSubmitted: true,
        message: true
      });
      this.showSuccess('password');
    } catch (e) {

    }

  }

  showSuccess(flag) {
    if (flag == 'password'){
      this.passwordMessages.show({severity: 'success', summary: '', detail: 'Updated Succesfully!'});
    }else{
      this.messages.show({severity: 'success', summary: '', detail: 'Updated Succesfully!'});
    }
  }

  render() {
    const { firstname, lastname, newPassword, confirmedPassword, nameSurnameSubmitted, passwordSubmitted, error, message, passwordError } = this.state;
    const { userinfo } = this.props;
    const { user } = this.props.user;

    return (
      <div>
          <h1 className="page-title">Settings</h1>
          <div className="page-box-wrap">
            <div className="page-box-wrap-inner">
              <h1 className="page-box-title">Personal Details</h1>
              <div className="row profile-details-box">
                <div className="col-md-5">
                  <div className="profile-userpic">
                    <img src={user.picture} className="img-responsive" alt=""/>
                  </div>
                </div>
                <div className="col-md-7">
                  <div className="profile-details">
                    <p className="profile-details-name">{userinfo.name ? userinfo.name : user.given_name} {userinfo.surname ? userinfo.surname : user.family_name}</p>
                    <p className="profile-details-email">{userinfo.email ? userinfo.email : user.email}</p>
                  </div>
                </div>
              </div>

              <div className={'form-group row' + (nameSurnameSubmitted && !firstname ? ' has-error' : '')}>
                  <label htmlFor="firstname" className="col-md-3 col-form-label">Firstname</label>
                  <div className="col-md-9">
                    <input type="text" className="form-control" name="firstname" placeholder={firstname ? firstname : (userinfo.name ? userinfo.name : user.given_name)} value={firstname} onChange={this.handleNameSurnameChange} />
                    {nameSurnameSubmitted && !firstname &&
                        <div className="help-block">Firstname is required</div>
                    }
                  </div>
              </div>
              <div className={'form-group row' + (nameSurnameSubmitted && !lastname ? ' has-error' : '')}>
                  <label htmlFor="lastname" className="col-md-3 col-form-label">Lastname</label>
                  <div className="col-md-9">
                    <input type="text" className="form-control" name="lastname" placeholder={lastname ? lastname : (userinfo.surname ? userinfo.surname : user.family_name)} value={lastname} onChange={this.handleNameSurnameChange} />
                    {nameSurnameSubmitted && !lastname &&
                        <div className="help-block">Lastname is required</div>
                    }
                  </div>
              </div>
              <Messages ref={(el) => this.messages = el} />
              <div className="form-group settings-update-form-group">
                  <button className="btn btn-primary" onClick={this.handleNameSurnameSubmit}>Update</button>
              </div>
            </div>
          </div>
          {(user.sub).startsWith("auth0")&&
          <div className="page-box-wrap">
            <div className="page-box-wrap-inner">
              <h1 className="page-box-title">Change Password</h1>
              <div className={'form-group row' + (passwordSubmitted && !newPassword ? ' has-error' : '')}>
                  <label htmlFor="newPassword" className="col-md-3 col-form-label">New</label>
                  <div className="col-md-9">
                    <input type="password" className="form-control" name="newPassword" value={newPassword} onChange={this.handlePasswordChange} />
                    {passwordSubmitted && !newPassword &&
                        <div className="help-block">Password is required</div>
                    }
                  </div>
              </div>
              <div className={'form-group row' + (passwordSubmitted && !confirmedPassword ? ' has-error' : '')}>
                  <label htmlFor="confirmedPassword"  className="col-md-3 col-form-label">Retype</label>
                  <div className="col-md-9">
                    <input type="password" className="form-control" name="confirmedPassword" value={confirmedPassword} onChange={this.handleConfirmedPasswordChange} />
                    {passwordSubmitted && !confirmedPassword &&
                        <div className="help-block">Confirmed Password is required</div>
                    }
                    {passwordError &&
                        <div className="help-block">Confirmed password is not the same with your new password</div>
                    }
                  </div>
              </div>
              <Messages ref={(el) => this.passwordMessages = el} />
              <div className="form-group settings-update-form-group">
                {(passwordError || !newPassword || !confirmedPassword) ?
                  (<button className="btn btn-primary" onClick={this.handlePasswordSubmit} disabled>Update</button>)
                  :
                  (<button className="btn btn-primary" onClick={this.handlePasswordSubmit}>Update</button>)
                }
              </div>
            </div>
          </div>
          }
      </div>
  );
  }
}

function mapStateToProps(state) {
    const { authentication, userinfo } = state;
    return {
        user: authentication, userinfo
    };
}

const connectedSettingsPage = connect(mapStateToProps)(SettingsPage);
export { connectedSettingsPage as SettingsPage };
