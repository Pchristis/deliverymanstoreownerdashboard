import React from 'react';
import { Link } from 'react-router-dom';
import { connect } from 'react-redux';
import { MultiSelect } from 'primereact/multiselect';
import {Calendar} from 'primereact/calendar';
import { Column } from "primereact/column";
import {ColumnGroup} from 'primereact/columngroup';
import {Row} from 'primereact/row';
import {DataTable} from 'primereact/datatable';
import {Button} from 'primereact/button';
import moment from 'moment';
import Highcharts from 'highcharts';
import HighchartsReact from "highcharts-react-official";

import { userActions } from '../../actions';
import { storesActions } from '../../actions';
import { invoicesActions } from '../../actions';

const months = ['January','February','March','April','May','June','July','August','September','October','November','December'];
  
class InvoicesPage extends React.Component {

  constructor(props) {
      super(props);

      let today = new Date();
      let month = today.getMonth();
      let year = today.getFullYear();
      let prevMonth = (month === 0) ? 11 : month - 1;
      let prevYear = (prevMonth === 11) ? year - 1 : year;
      let minDate = new Date();
      minDate.setMonth(prevMonth);
      minDate.setFullYear(prevYear);
      let maxDate = new Date();

      if (localStorage.getItem('deliveryman-dateRange')){
        let dateRange = JSON.parse(localStorage.getItem('deliveryman-dateRange'));
        minDate = new Date(dateRange[0]);
        maxDate = new Date(dateRange[1]);
      }

      this.state = {
          startDate: null,
          endDate: null,
          changeStatus: false,
          stores: null,
          storesIds: null,
          selectedInvoices: [],
          dateRange: [minDate, maxDate]
      };

      const { dispatch } = this.props;

      dispatch(invoicesActions.loginToApiCall());

      this.onDateRangeChange = this.onDateRangeChange.bind(this);
      this.changeDateFormat = this.changeDateFormat.bind(this);
      this.onStoreChange = this.onStoreChange.bind(this);
      this.shareLinkInvoice = this.shareLinkInvoice.bind(this);
      this.downloadInvoicePDf = this.downloadInvoicePDf.bind(this);
      this.downloadZipSubmit = this.downloadZipSubmit.bind(this);
      this.selectInvoice = this.selectInvoice.bind(this);
      this.downloadOrdersBreakDown = this.downloadOrdersBreakDown.bind(this);
      this.handleInvoicesSubmit = this.handleInvoicesSubmit.bind(this);
  }

  componentDidMount(){
    const { dispatch } = this.props;

    this.setState({
      startDate: moment(Date.parse(this.state.dateRange[0])).format("Y-MM-DD"),
      endDate: moment(Date.parse(this.state.dateRange[1])).format("Y-MM-DD")
    });

  }

  componentDidUpdate(){
    if (this.state.changeStatus == false){
      setTimeout(function() {
        document.getElementById("filterButton").click();
      }.bind(this), 500);
      this.setState({
        changeStatus: true
      });
    }
  }

  selectInvoice(item,event){
    let selectedItem = item['invoiceid'];
    let { selectedInvoices } = this.state;
    if (!selectedInvoices.includes(selectedItem)){
      selectedInvoices.push(selectedItem);
    }else{
      var index = selectedInvoices.indexOf(selectedItem);
      if (index > -1) {
        selectedInvoices.splice(index, 1);
      }
    }

    this.setState({
      selectedInvoices
    });
  }

  downloadOrdersBreakDown(item,event){
    const link = document.createElement('a');
    link.href = item['ordersBreakdownURL'];
    link.target = "_blank";
    document.body.appendChild(link);
    link.click();
  }

  downloadZipSubmit(){
    let { selectedInvoices } = this.state;
    let selectedInvoicesJson = JSON.stringify(selectedInvoices);
    invoicesActions.downloadZip(selectedInvoicesJson);
  }

  changeDateFormat(date) {
      let month = new Date(date).getMonth();
      month = months[month];
      let day = new Date(date).getDate();
      let year = new Date(date).getFullYear();
      let finalDate = month + ' ' + day + ', ' + year;
      return finalDate;
  }

  onStoreChange(e) {
        this.setState({stores: e.value});
        const storesIds = [];
        e.value.map((store, key) =>
            storesIds.push(store.id)
        );
        this.setState({
          storesIds: storesIds,
          changeStatus: true
        });
  }

  onDateRangeChange(e) {
    this.setState({
      dateRange: e.value,
      startDate: moment(Date.parse(e.value[0])).format("Y-MM-DD"),
      endDate: moment(Date.parse(e.value[1])).format("Y-MM-DD"),
      changeStatus: true
    });
    localStorage.setItem('deliveryman-dateRange', JSON.stringify(e.value));
  }

  shareLinkInvoice(item,event) {
      invoicesActions.shareLinkOfInvoice(item['invoiceid']);
  }

  downloadInvoicePDf(item,event) {
    let filename = item['organization'] + ' - ' + item['invoiceid'];
    invoicesActions.getInvoicePDF(item['invoiceid'],filename);
  }

  handleInvoicesSubmit(e) {
    e.preventDefault();

    let { startDate, endDate, storesIds } = this.state;
    const { dispatch } = this.props;
    const { userinfo } = this.props;

    let storesIdsArray = [];

    if (storesIds == null){
      if ('freshbookData' in userinfo){
        Object.keys(userinfo['freshbookData']).forEach(function(key) {
          storesIdsArray.push(userinfo['freshbookData'][key]['freshbookId']);
        })
      }
    }else{
      storesIdsArray = storesIds;
    }
    if (startDate && endDate) {
      dispatch(invoicesActions.filterInvoices(startDate, endDate, storesIdsArray));
    }

  }

  render() {
      const { userinfo } = this.props;
      const { overview } = this.props;
      const { invoices } = this.props;

      const { selectedInvoices } = this.state;

      const storeArray = [];

      if ('freshbookData' in userinfo){
        Object.keys(userinfo['freshbookData']).forEach(function(key) {
          let storeDetails = {};
          storeDetails['id'] = userinfo['freshbookData'][key]['freshbookId'];
          storeDetails['name'] = userinfo['freshbookData'][key]['storeName'];
          storeArray.push(storeDetails);
        })
      }

      let invoicesDATA = [];
      let footerGroup = '';

      let amountTotal = 0;
      let paidTotal = 0;
      let outstandingTotal = 0;
      if (invoices && invoices.length){
        invoices.map((invoice, key) => {
          if (invoice['display_status'] !== "draft"){
            var regex = /(https.*?\.pdf)/g;
            var foundURL = invoice['notes'].match(regex);
            let invoiceRow = {};
            invoiceRow['organization'] = invoice['organization'];
            invoiceRow['invoiceid'] = invoice['invoiceid'];
            invoiceRow['create_date'] = invoice['create_date'];
            invoiceRow['amount'] = invoice['amount']['amount'];
            invoiceRow['outstanding'] = invoice['outstanding']['amount'];
            invoiceRow['paid'] = invoice['paid']['amount'];
            invoiceRow['paymentStatus'] = invoice['payment_status'];
            invoiceRow['ordersBreakdownURL'] = foundURL ? foundURL : '';
            invoicesDATA.push(invoiceRow);

            amountTotal = amountTotal + parseFloat(invoice['amount']['amount']);
            paidTotal = paidTotal + parseFloat(invoice['outstanding']['amount']);
            outstandingTotal = outstandingTotal + parseFloat(invoice['paid']['amount']);
          }
        });
      }

      footerGroup = <ColumnGroup>
                      <Row>
                        <Column footer="" colSpan={3}/>
                        <Column footer="Total"  />
                        <Column footer={"€" + amountTotal.toFixed(2)}/>
                        <Column footer={"€" + paidTotal.toFixed(2)}/>
                        <Column footer={"€" + outstandingTotal.toFixed(2)}/>
                        <Column footer="" colSpan={2}/>
                      </Row>
                   </ColumnGroup>;


      return (
        <div>
          <div className="col-md-12">
            <div className="row filter-section">
              <div className="col-md-3">
                <Calendar value={this.state.dateRange} onChange={this.onDateRangeChange} selectionMode="range" readonlyInput={true} dateFormat="dd/mm/yy" showIcon="true" icon="pi pi-calendar" />
              </div>
              <div className="col-md-3">
                  {storeArray.length ? (
                      <MultiSelect value={this.state.stores} optionLabel="name" options={storeArray} onChange={this.onStoreChange} placeholder="Select a Store"/>
                  ) : (
                      <MultiSelect value={this.state.stores} optionLabel="name" options={storeArray} onChange={this.onStoreChange} placeholder="Select a Store" disabled />
                  )}
              </div>
              <div className="col-md-3">
                  {storeArray.length ? (
                      <button className="btn btn-primary" id="filterButton" onClick={this.handleInvoicesSubmit}>Filter</button>
                  ) : (
                      <button className="btn btn-primary" id="filterButton" onClick={this.handleInvoicesSubmit} disabled>Filter</button>
                  )}
              </div>
              <div className="col-md-3 text-right">
              {selectedInvoices.length > 0 && (
                <Button id={'zip-loader'} icon="pi pi-download" onClick={this.downloadZipSubmit} label="Mass Download" />
              )}
            </div>
            </div>
          </div>
          <div className="col-md-12">
            <h2>Freshbooks Invoices</h2>
            {(invoices && invoices.length) ? (
              <DataTable style={{textAlign:'center'}} value={invoicesDATA} responsive={true} id="invoice-table" paginator={true} rows={10} footerColumnGroup={footerGroup}>
                <Column style={{width: '6em'}} body={(item) => <input type="checkbox" onChange={(event) => {this.selectInvoice(item, event);}} />} />
                <Column field="organization" header="Organization" />
                <Column field="invoiceid" header="Invoice ID" body={(item) => item.invoiceid} />
                <Column field="create_date" header="Issued On" body={(item) => this.changeDateFormat(item.create_date)}/>
                <Column field="amount" header="Total Amount" body={(item) => "€" + item.amount}/>
                <Column field="outstanding" header="Outstanding Amount" body={(item) => "€" + item.outstanding}/>
                <Column field="paid" header="Paid Amount" body={(item) => "€" + item.paid}/>
                <Column field="paymentStatus" header="Payment Status"/>
                <Column header="Actions" body={(item) =>
                  <div className="actionButtons">
                    <Button id={item['invoiceid'] + '-loader'} icon="pi pi-download" onClick={(event) => {this.downloadInvoicePDf(item, event);}} />
                    <Button icon="pi pi-print" onClick={(event) => {this.shareLinkInvoice(item, event);}} />
                    <Button label="Orders Breakdown" icon="pi pi-download" onClick={(event) => {this.downloadOrdersBreakDown(item, event);}} />
                  </div>
                } />
              </DataTable>
              ) : <p>No results found!</p>
            }
          </div>
        </div>
      );
  }
}

function mapStateToProps(state) {
    const { authentication, userinfo, overview, invoices } = state;
    return {
        user: authentication, userinfo, overview, invoices
    };
}

const connectedInvoicesPage = connect(mapStateToProps)(InvoicesPage);
export { connectedInvoicesPage as InvoicesPage };
