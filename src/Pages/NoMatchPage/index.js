import React from 'react';
import { Link } from 'react-router-dom';

// import { userActions } from '../actions';

class NoMatchPage extends React.Component {

    render() {
        return (
          <div  className="container" id="page-404">
            <div className="flex-block">
              <div className="col-md-12 top-logo">
                <a href="/"><img src="/assets/images/logo.svg" alt="DeliveryMan" id="login-logo"/></a>
              </div>
              <div className="col-md-6 flex-block-404">
                <div className="box-404">
                    <h1>404</h1>
                    <p className="description-404">Ops! Look’s like your lost, go back or press the button below.</p>
                    <Link to="/overview" onClick={() => window.location.refresh()} className="btn btn-primary" id="takeMeBack">Take me Back!</Link>
                    <p className="small-description-404">Why am I seeing this? Lorem ipsum dolor sit amet, consectetur adipiscing elit. Duis in elementum ex. Cras efficitur lectus viverra, sollicitudin ligula eget, varius magna. Quisque ut justo vulputate, aliquam diam at, pellentesque leo.</p>
                </div>
              </div>
              <div className="col-md-6 flex-block-404" id="photo-404"></div>
              <div className="col-md-12 bottom-circles"></div>
            </div>
          </div>
        );
    }
}

export default NoMatchPage;
