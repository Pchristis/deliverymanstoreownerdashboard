import React from 'react';
import Text from 'react';
import { Link } from 'react-router-dom';
import { connect } from 'react-redux';
import { Dropdown } from 'primereact/dropdown';
import {Calendar} from 'primereact/calendar';
import {TreeTable} from 'primereact/treetable';
import { Column } from "primereact/column";
import {ColumnGroup} from 'primereact/columngroup';
import {Row} from 'primereact/row';
import {OverlayPanel} from 'primereact/overlaypanel';
import {DataTable} from 'primereact/datatable';
import {SelectButton} from 'primereact/selectbutton';
import {Button} from 'primereact/button';
import Iframe from 'react-iframe'

import { userActions } from '../../actions';
import { storesActions } from '../../actions';
import { analysisActions } from '../../actions';

const months = ['January','February','March','April','May','June','July','August','September','October','November','December'];
let treetableExpandedKeys = [];

class AnalysisPage extends React.Component {

  constructor(props) {
      super(props);

      let today = new Date();
      let month = today.getMonth();
      let year = today.getFullYear();
      let prevMonth = (month === 0) ? 11 : month - 1;
      let prevYear = (prevMonth === 11) ? year - 1 : year;
      let minDate = new Date();
      minDate.setMonth(prevMonth);
      minDate.setFullYear(prevYear);
      let maxDate = new Date();

      if (localStorage.getItem('deliveryman-dateRange')){
        let dateRange = JSON.parse(localStorage.getItem('deliveryman-dateRange'));
        minDate = new Date(dateRange[0]);
        maxDate = new Date(dateRange[1]);
      }

      let selectedStore = null;

      if (localStorage.getItem('deliveryman-store')){
        selectedStore = JSON.parse(localStorage.getItem('deliveryman-store'));
      }

      this.state = {
            store: selectedStore,
            startDateTS: null,
            endDateTS: null,
            selectedItem: null,
            dateRange: [minDate, maxDate],
            filterButtons: null,
            printComponent: false,
            changeStatus: false
        };

        this.onStoreChange = this.onStoreChange.bind(this);
        this.onDateRangeChange = this.onDateRangeChange.bind(this);
        this.handleAnalysisSubmit = this.handleAnalysisSubmit.bind(this);
        this.clickOrderNumber = this.clickOrderNumber.bind(this);
        this.filterCategoryBreakdown = this.filterCategoryBreakdown.bind(this);
        this.printComponentView = this.printComponentView.bind(this);
        this.changeDateFormat = this.changeDateFormat.bind(this);
        this.changeTimeFormat = this.changeTimeFormat.bind(this);
        this.rowClassName = this.rowClassName.bind(this);
        this.download_csv = this.download_csv.bind(this);
        this.export_table_to_csv = this.export_table_to_csv.bind(this);
        this.exportToCSV = this.exportToCSV.bind(this);
  }

  componentDidMount(){
    const { dispatch } = this.props;
    dispatch(storesActions.getStores());
    this.setState({
      startDateTS: Date.parse(this.state.dateRange[0]),
      endDateTS: Date.parse(this.state.dateRange[1])
    });

  }

  componentDidUpdate(){
    if (this.state.changeStatus == false){
      if (this.state.store == null){
        try {

          const { dispatch } = this.props;

          if (this.props.stores){
            setTimeout(function() {
              if (this.state.store == null){
                this.setState({
                  store: this.props.stores[0]
                });
                localStorage.setItem('deliveryman-store', JSON.stringify(this.props.stores[0]));
              }

              setTimeout(function() {
                  document.getElementById("filterButton").click();
              }.bind(this), 500);
            }.bind(this), 500);
          }

        } catch (e) {

        }
      }else{
        setTimeout(function() {
            document.getElementById("filterButton").click();
        }.bind(this), 500);
      }
      this.setState({
        changeStatus: true
      });
    }
  }

  download_csv(csv, filename) {
    var csvFile;
    var downloadLink;
    // CSV FILE
    csvFile = new Blob([csv], {type: "text/csv"});
    // Download link
    downloadLink = document.createElement("a");
    // File name
    downloadLink.download = filename;
    // We have to create a link to the file
    downloadLink.href = window.URL.createObjectURL(csvFile);
    // Make sure that the link is not displayed
    downloadLink.style.display = "none";
    // Add the link to your DOM
    document.body.appendChild(downloadLink);
    downloadLink.click();
  }

  export_table_to_csv(html, filename) {
    var csv = [];
    var rows = html;

    for (var i = 0; i < rows.length; i++) {
      var row = [], cols = rows[i].querySelectorAll("td, th");

      for (var j = 0; j < cols.length; j++)
          row.push(cols[j].innerText);

      csv.push(row.join(","));
    }

    // Download CSV
    this.download_csv(csv.join("\n"), filename);
  }

  exportToCSV() {
    let breakdownHtml = document.querySelectorAll("#treeToPrint table tr");
    this.export_table_to_csv(breakdownHtml, "Category BreakDown.csv");

    let paymentsHtml = document.querySelectorAll("#tableToPrint table tr");
    this.export_table_to_csv(paymentsHtml, "Payments BreakDown.csv");
  }

  rowClassName(node) {
        return {'parentNode' : (node.children)};
    }

  onDateRangeChange(e) {
    this.setState({
      dateRange: e.value,
      startDateTS: Date.parse(e.value[0]),
      endDateTS: Date.parse(e.value[1]),
      changeStatus: true
    });

    localStorage.setItem('deliveryman-dateRange', JSON.stringify(e.value));
  }


  changeDateFormat(date) {
      let month = new Date(date).getMonth();
      month = months[month];
      let day = new Date(date).getDate();
      let year = new Date(date).getFullYear();
      let finalDate = month + ' ' + day + ', ' + year;
      return finalDate;
  }

  changeTimeFormat(date) {
      let hours = new Date(date).getHours();
      let minutes = new Date(date).getMinutes();
      let seconds = new Date(date).getSeconds();
      let finalTime = hours + ':' + minutes + ':' + seconds;
      return finalTime;
  }

  printComponentView() {
    document.getElementById("tableNotToPrint").style.display = "none";
    document.getElementById("tableToPrint").style.display = "block";
    document.getElementById("treeNotToPrint").style.display = "none";
    document.getElementById("treeToPrint").style.display = "block";
    document.getElementById("filter-section").style.display = "none";

    setTimeout(function() {
      window.print();
    }, 200);

    window.onafterprint = function() {
      document.getElementById("tableToPrint").style.display = "none";
      document.getElementById("tableNotToPrint").style.display = "block";
      document.getElementById("treeToPrint").style.display = "none";
      document.getElementById("treeNotToPrint").style.display = "block";
      document.getElementById("filter-section").style.display = "flex";
    };

  }

  clickOrderNumber(item,event) {
    this.setState({
      selectedItem: item
    });
  }

  filterCategoryBreakdown(date,e) {
    e.preventDefault();
    const { store } = this.state;
    const { dispatch } = this.props;

    let today = new Date();
    this.setState({
      filterButtons: date,
    });

    switch (date) {
      case 'daily':
        this.setState({
          dateRange: [today, today],
          startDateTS: Date.parse(today),
          endDateTS: Date.parse(today)
        });
        if (store) {
          dispatch(analysisActions.filterAnalysis(Date.parse(today), Date.parse(today), store.id));
          dispatch(analysisActions.orderAnalysis(Date.parse(today), Date.parse(today), store.id));
        }
        break;
      case 'weekly':
        let lastWeek = new Date(new Date().setDate(new Date().getDate() - 7));
        this.setState({
          dateRange: [lastWeek, today],
          startDateTS: Date.parse(lastWeek),
          endDateTS: Date.parse(today)
        });
        if (store) {
          dispatch(analysisActions.filterAnalysis(Date.parse(lastWeek), Date.parse(today), store.id));
          dispatch(analysisActions.orderAnalysis(Date.parse(lastWeek), Date.parse(today), store.id));
        }
        break;
      case 'monthly':
        let lastmonth = new Date(new Date().setMonth(new Date().getMonth() - 1));
        this.setState({
          dateRange: [lastmonth, today],
          startDateTS: Date.parse(lastmonth),
          endDateTS: Date.parse(today)
        });
        if (store) {
          dispatch(analysisActions.filterAnalysis(Date.parse(lastmonth), Date.parse(today), store.id));
          dispatch(analysisActions.orderAnalysis(Date.parse(lastmonth), Date.parse(today), store.id));
        }
        break;
    }

  }

  onStoreChange(e) {
      this.setState({
        store: e.value,
        changeStatus: true
      });

      localStorage.setItem('deliveryman-store', JSON.stringify(e.value));
  }

  handleAnalysisSubmit(e) {
    e.preventDefault();

    let { startDateTS, endDateTS, store } = this.state;
    const { dispatch } = this.props;

    if (startDateTS && endDateTS && store) {
      dispatch(analysisActions.filterAnalysis(startDateTS, endDateTS, store.id));
      dispatch(analysisActions.orderAnalysis(startDateTS, endDateTS, store.id));
    }

  }

  render() {
      const { userinfo } = this.props;
      const { stores } = this.props;
      const { analysis } = this.props;
      const { orderanalysis } = this.props;

      let orderAnalysisFirstColumn = {
                                      "0-5EUR": "0-5.99",
                                      "6-10EUR": "6-10.99",
                                      "11-20EUR": "11-20.99",
                                      "21-30EUR": "21-30.99",
                                      "31-50EUR": "31-50.99",
                                      "51EUR+": "51 +"
                                    }

      let dateRange = this.changeDateFormat(this.state.startDateTS) + ' - ' + this.changeDateFormat(this.state.endDateTS);

      const filterButtons = [
        {label: 'Daily', value: 'daily'},
        {label: 'Weekly', value: 'weekly'},
        {label: 'Monthly', value: 'monthly'}
      ];

      const categoryBreakdown = [];

      if ('CategoryBreakdown' in analysis){
        let parentsCounter = 0;
        treetableExpandedKeys = [];
        analysis['CategoryBreakdown'].map((category, key) => {
          treetableExpandedKeys.push("true");
          let categoryJSON = {};
          let categoryDetails = {};
          categoryDetails['name'] = category['name'];
          let categoryDetailsOrders = 0;
          let categoryDetailsIncome = 0;
          categoryJSON['children'] = [];

          let childrensCounter = 1;
          Object.keys(category['item_records']).forEach(function(key2) {
            let childrenJSON = {};
            let childrenDetails = {};
            childrenDetails['name'] = key2;
            childrenDetails['sold'] = category['item_records'][key2][0]['sold'];
            categoryDetailsOrders = categoryDetailsOrders + category['item_records'][key2][0]['sold'];
            childrenDetails['income'] = '€'+category['item_records'][key2][0]['income'].toFixed(2);
            categoryDetailsIncome = categoryDetailsIncome + category['item_records'][key2][0]['income'];
            childrenJSON['data'] = childrenDetails;
            childrenJSON['key'] = parentsCounter + '-' + childrensCounter;
            categoryJSON['children'].push(childrenJSON);
            childrensCounter = childrensCounter + 1;
          })
          categoryDetails['sold'] = "Orders: " + categoryDetailsOrders;
          categoryDetails['income'] = "Amount: €" + categoryDetailsIncome.toFixed(2);
          categoryJSON['key'] = parentsCounter;
          categoryJSON['data'] = categoryDetails;
          categoryBreakdown.push(categoryJSON);
          parentsCounter = parentsCounter + 1;

        })

      }

      let footerGroup = '';

      let headerGroup = <ColumnGroup>
                                      <Row>
                                        <Column header="Order Number (1)"/>
                                        <Column header="Total Order Amount (2)"/>
                                        <Column header="Delivery (incl. VAT) (3)" />
                                        <Column header="Bag (incl. VAT) (4)" />
                                        <Column header="VAT (5)" />
                                        <Column header="Commission (incl. 19% VAT) (6)"/>
                                        <Column header="Net to Vendor (7)" />
                                        <Column header="Date (8)" />
                                        <Column header="Payment (9)" />
                                        <Column header="Shop (10)" />
                                      </Row>
                                      <Row>
                                        <Column header="Order Number ID"/>
                                        <Column header="Total amount of the order"/>
                                        <Column header="Delivery including 19% VAT" />
                                        <Column header="Bag including 19% VAT" />
                                        <Column header="VAT of the order's items only" />
                                        <Column header="((2) - (3) - (4) - (5)) * Store Commission Percentage * 1.19%"/>
                                        <Column header="(2) - (6) - (3) if the order's delivery was handled by DM" />
                                        <Column header="Date that the order was entered" />
                                        <Column header="Payment - Cash" />
                                        <Column header="Shop's Name" />
                                      </Row>
                                   </ColumnGroup>;

      if ('PaymentsBreakdown' in analysis){
        let chargedTotal = 0;
        let deliveryTotal = 0;
        let bagTotal = 0;
        let vatTotal = 0;
        let commissionTotal = 0;
        let netTotal = 0;
        analysis['PaymentsBreakdown'].map((payment, key) => {
          chargedTotal = chargedTotal + payment['charged'];
          deliveryTotal = deliveryTotal + payment['delivery'];
          bagTotal = bagTotal + payment['bags'];
          vatTotal = vatTotal + payment['vat'];
          commissionTotal = commissionTotal + payment['commission'];
          netTotal = netTotal + payment['net_remain'];
        });


        footerGroup = <ColumnGroup>
                                <Row>
                                    <Column footer="Total"  />
                                    <Column footer={"€" + chargedTotal.toFixed(2)}/>
                                    <Column footer={"€" + deliveryTotal.toFixed(2)}/>
                                    <Column footer={"€" + bagTotal.toFixed(2)}/>
                                    <Column footer={"€" + vatTotal.toFixed(2)}/>
                                    <Column footer={"€" + commissionTotal.toFixed(2)}/>
                                    <Column footer={"€" + netTotal.toFixed(2)}/>
                                    <Column footer="" colSpan={3}/>
                                </Row>
                             </ColumnGroup>;
       }


      return (
        <div className="col-md-12">
          <div className="row filter-section" id="filter-section">
            <div className="col-md-3">
              <Calendar value={this.state.dateRange} onChange={this.onDateRangeChange} selectionMode="range" readonlyInput={true} dateFormat="dd/mm/yy" showIcon="true" icon="pi pi-calendar" />
            </div>
            <div className="col-md-3">
            {(stores && stores.length) ? (
              <Dropdown value={this.state.store} optionLabel="name" options={stores} onChange={this.onStoreChange} placeholder="Select a Store"/>
            ) : ''}
            </div>
            <div className="col-md-3">
                {this.state.store ? (
                    <button className="btn btn-primary" id="filterButton" onClick={this.handleAnalysisSubmit}>Filter</button>
                ) : (
                    <button className="btn btn-primary" id="filterButton" onClick={this.handleAnalysisSubmit} disabled>Filter</button>
                )}
            </div>
            {'CategoryBreakdown' in analysis && (
            <div className="col-md-3 text-right exportButtonsDiv">
              <Button onClick={this.printComponentView} icon="pi pi-print"/>
              <Button type="button" icon="pi pi-external-link" iconPos="left" label="CSV" onClick={this.exportToCSV}></Button>
            </div>
            )}
          </div>

          {'Sales' in analysis &&
          <div>
            <div className="analysis-section">
              <div className="row" id="analysisOverallSales">
                <div className="col-md-2">
                  <div className="description">Overall sales</div>
                  <div className="price">€{analysis['Sales']['overall_sales'].toFixed(2)}</div>
                </div>
                <div className="col-md-2">
                  <div className="description">Net sales</div>
                  <div className="price">€{analysis['Sales']['net_sales'].toFixed(2)}</div>
                </div>
                <div className="col-md-2">
                  <div className="description">Commission paid</div>
                  <div className="price">€{analysis['Sales']['commission_paid'].toFixed(2)}</div>
                </div>
                <div className="col-md-2">
                  <div className="description">Total orders</div>
                  <div className="price">{analysis['Sales']['total_orders']}</div>
                </div>
                <div className="col-md-4">
                  <div className="description">Average [net sales / orders]</div>
                  <div className="price">€{(analysis['Sales']['average_price_per_order'] ? analysis['Sales']['average_price_per_order'] : 0).toFixed(2)}</div>
                </div>
              </div>
            </div>
          </div>
          }

          {'CategoryBreakdown' in analysis &&
            <div className="paddingBottom">
                <div className="paddingBottom">
                  <SelectButton value={this.state.filterButtons} options={filterButtons} onChange={(event) => {this.filterCategoryBreakdown(event.value, event)}}></SelectButton>
                </div>
                <div id="analysisDates">{dateRange}</div>
            </div>
          }

          {orderanalysis &&
            <div className="paddingBottom orderanalysistableDiv">
                <h3 className="section-title">Orders value breakdown</h3>
                <table class="orderanalysistable">
                  <thead>
                    <tr>
                      <th>Range [EUR]</th>
                      <th>Quantity [transactions]</th>
                      <th>Net Sales [EUR]</th>
                      <th>Quantity Ratio [%]</th>
                      <th>Net Sales Ratio [%]</th>
                    </tr>
                  </thead>
                  <tbody>
                  {Object.entries(orderanalysis).map(([keyAnalysis, orderanalysisValue]) => (
                    <tr>
                      <td>{orderAnalysisFirstColumn[keyAnalysis]}</td>
                      <td>{orderanalysisValue[0]['transaction']}</td>
                      <td>{orderanalysisValue[0]['sales']}</td>
                      <td>{orderanalysisValue[0]['transaction%'] == null ? 0 : orderanalysisValue[0]['transaction%']}</td>
                      <td>{orderanalysisValue[0]['sales%'] == null ? 0 : orderanalysisValue[0]['sales%']}</td>
                    </tr>
                  ))}

                  </tbody>
                </table>
            </div>
          }

          {'CategoryBreakdown' in analysis &&
            <div className="paddingBottom">
                <h3 className="section-title">Category breakdown</h3>

                <TreeTable id="treeNotToPrint" value={categoryBreakdown} rowClassName={this.rowClassName} responsive={true} paginator={true} rows={10}>
                  <Column field="name" header="Name" expander></Column>
                  <Column field="sold" header="Sold"></Column>
                  <Column field="income" header="Income"></Column>
                </TreeTable>
                <TreeTable id="treeToPrint" value={categoryBreakdown} rowClassName={this.rowClassName} style={{display:'none'}} expandedKeys={treetableExpandedKeys}
                    onToggle={e => this.setState({expandedKeys: e.value})}>
                  <Column field="name" header="Name" expander></Column>
                  <Column field="sold" header="Sold"></Column>
                  <Column field="income" header="Income"></Column>
                </TreeTable>
            </div>
          }

          {'PaymentsBreakdown' in analysis &&
            <div className="paymentBreakdownDiv">
              <h3 className="section-title">Payments breakdown</h3>

              <DataTable id="tableNotToPrint" value={analysis['PaymentsBreakdown']} paginator={true} rows={10} headerColumnGroup={headerGroup} footerColumnGroup={footerGroup}>
                    <Column field="order_number" body={(item) => <div>
                      <button className="btn btn-link" onClick={(event) => {this.clickOrderNumber(item, event); this.op.toggle(event);}}>{item.order_number}</button>
                      <p style={{fontSize:11,margin:0}}>{item.couponCode?"Coupon: "+item.couponCode:null}</p>
                      <p style={{fontSize:11,margin:0}}>{item.pickupTrackUrl?"Click to track!":null}</p>
                    </div>}/>
                    <Column field="charged" body={(item) => "€" + item.charged.toFixed(2)}/>
                    <Column field="delivery" body={(item) => "€" + item.delivery.toFixed(2)}/>
                    <Column field="bags" body={(item) => "€" + item.bags.toFixed(2)}/>
                    <Column field="vat" body={(item) => "€" + item.vat.toFixed(2)}/>
                    <Column field="commission" body={(item) => "€" + item.commission.toFixed(2)}/>
                    <Column field="net_remain" body={(item) => "€" + item.net_remain.toFixed(2)}/>
                    <Column field="date" body={(item) => this.changeDateFormat(item.date)}/>
                    <Column field="payment_method"/>
                    <Column body={(item) => this.state.store.name}/>
              </DataTable>

              <DataTable id="tableToPrint" value={analysis['PaymentsBreakdown']} style={{display:'none'}} headerColumnGroup={headerGroup} footerColumnGroup={footerGroup}>
                    <Column field="order_number" body={(item) => <button className="btn btn-link" onClick={(event) => {this.clickOrderNumber(item, event); this.op.toggle(event);}}>{item.order_number}</button>} />
                    <Column field="charged" body={(item) => "€" + item.charged.toFixed(2)}/>
                    <Column field="delivery" body={(item) => "€" + item.delivery.toFixed(2)}/>
                    <Column field="bags" body={(item) => "€" + item.bags.toFixed(2)}/>
                    <Column field="vat" body={(item) => "€" + item.vat.toFixed(2)}/>
                    <Column field="commission" body={(item) => "€" + item.commission.toFixed(2)}/>
                    <Column field="net_remain" body={(item) => "€" + item.net_remain.toFixed(2)}/>
                    <Column field="date" body={(item) => this.changeDateFormat(item.date)}/>
                    <Column field="payment_method" />
                    <Column body={(item) => this.state.store.name}/>
              </DataTable>

              <OverlayPanel ref={(el) => this.op = el} showCloseIcon={true} className="customPopup" id={this.state.selectedItem && this.state.selectedItem.pickupTrackUrl?"popupAnalysisLarge":"popupAnalysisSmall"}>
                {this.state.selectedItem && (
                  <div>
                    <div className="order-number">{this.state.selectedItem.order_number}</div>
                    <div className="order-number paddingBottom">{this.changeDateFormat(this.state.selectedItem.date)} {this.changeTimeFormat(this.state.selectedItem.date)}</div>
                    <div className="client-name">{this.state.selectedItem.order_details.full_name}</div>
                    <div className="client-address">{this.state.selectedItem.order_details.address}</div>
                    <div className="order-details-title paddingTop">Order Details</div>
                    {this.state.selectedItem.order_details.order_items.map((orderItem, key) => {
                      return <div className="order-item">{orderItem.quantity} x {orderItem.item}</div>
                    })}
                    <div className="row paddingTop">
                      <div className="col-md-2">
                        <div className="order-description">VAT</div>
                        <div className="order-description">Delivery</div>
                        <div className="order-description-total">Total</div>
                      </div>
                      <div className="col-md-2">
                        <div className="order-vat">€{this.state.selectedItem.vat.toFixed(2)}</div>
                        <div className="order-vat">€{this.state.selectedItem.delivery.toFixed(2)}</div>
                        <div className="order-vat">€{this.state.selectedItem.charged.toFixed(2)}</div>
                      </div>
                    </div>
                    {this.state.selectedItem.pickupTrackUrl ?
                      <div className="paddingTop">
                        <Iframe url={this.state.selectedItem.pickupTrackUrl}
                          width="870px"
                          height="500px"
                          display="initial"
                          position="relative"
                        />
                      </div>
                    :null}
                  </div>
                  )}
              </OverlayPanel>
            </div>
          }
        </div>
      );
  }
}

function mapStateToProps(state) {
    const { authentication, userinfo, stores, analysis, orderanalysis } = state;
    return {
        user: authentication, userinfo, stores, analysis, orderanalysis
    };
}

const connectedAnalysisPage = connect(mapStateToProps)(AnalysisPage);
export { connectedAnalysisPage as AnalysisPage };
