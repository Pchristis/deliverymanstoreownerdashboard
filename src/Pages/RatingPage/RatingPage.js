import React from 'react';
import { Link } from 'react-router-dom';
import { connect } from 'react-redux';
import { Dropdown } from 'primereact/dropdown';
import {Button} from 'primereact/button';
import StarRatings from 'react-star-ratings';

import { ratingsActions } from '../../actions';

const months = ['January','February','March','April','May','June','July','August','September','October','November','December'];

class RatingPage extends React.Component {

  constructor(props) {
      super(props);

      let selectedStore = null;

      if (localStorage.getItem('deliveryman-store')){
        selectedStore = JSON.parse(localStorage.getItem('deliveryman-store'));
      }

      let selectedRatingMoreThan = {label: 'All Ratings', value: 0};

      if (localStorage.getItem('deliveryman-rating-more-than')){
        selectedRatingMoreThan = JSON.parse(localStorage.getItem('deliveryman-rating-more-than'));
      }else{
        localStorage.setItem('deliveryman-rating-more-than', JSON.stringify(selectedRatingMoreThan));
      }

      let selectedLowToHigh = {label: 'Low To High', value: 0};

      if (localStorage.getItem('deliveryman-low-to-high')){
        selectedLowToHigh = JSON.parse(localStorage.getItem('deliveryman-low-to-high'));
      }else{
        localStorage.setItem('deliveryman-low-to-high', JSON.stringify(selectedLowToHigh));
      }

      this.state = {
            store: selectedStore,
            selectedRatingMoreThan : selectedRatingMoreThan,
            ratingsMoreThan : [
                {label: 'All Ratings', value: 0},
                {label: 'Rating > 1', value: 1},
                {label: 'Rating > 2', value: 2},
                {label: 'Rating > 3', value: 3},
                {label: 'Rating > 4', value: 4}
            ],
            selectedLowToHigh : selectedLowToHigh,
            lowToHigh : [
              {label: 'Low To High', value: 0},
              {label: 'High To Low', value: 1},
            ],
            changeStatus: false
      };


      if (this.state.changeStatus == false){

        if ((this.props.stores) && (this.state.store == null)){
          setTimeout(function() {
            this.setState({
              store: this.props.stores[0]
            });
            localStorage.setItem('deliveryman-store', JSON.stringify(this.props.stores[0]));
          }.bind(this), 500);
        }

        if (this.state.store){
          setTimeout(function() {
            document.getElementById("filterButton").click();
          }.bind(this), 500);

          this.setState({
            changeStatus: true
          });
        }
      }

      this.onStoreChange = this.onStoreChange.bind(this);
      this.onRatingsMoreThanChange = this.onRatingsMoreThanChange.bind(this);
      this.onLowToHighChange = this.onLowToHighChange.bind(this);
      this.handleRatingSubmit = this.handleRatingSubmit.bind(this);
      this.changeDateFormat = this.changeDateFormat.bind(this);
  }

  changeDateFormat(date) {
      let month = new Date(date).getMonth();
      month = months[month];
      let day = new Date(date).getDate();
      let year = new Date(date).getFullYear();
      let finalDate = month + ' ' + day + ', ' + year;
      return finalDate;
  }

  onStoreChange(e) {
      this.setState({
        store: e.value,
        changeStatus: true
      });

      localStorage.setItem('deliveryman-store', JSON.stringify(e.value));
  }

  onRatingsMoreThanChange(e) {
      this.setState({
        selectedRatingMoreThan: e.value,
        changeStatus: true
      });

      localStorage.setItem('deliveryman-rating-more-than', JSON.stringify(e.value));
  }

  onLowToHighChange(e) {
      this.setState({
        selectedLowToHigh: e.value,
        changeStatus: true
      });

      localStorage.setItem('deliveryman-low-to-high', JSON.stringify(e.value));
  }

  handleRatingSubmit(e) {
    e.preventDefault();

    let { store, selectedRatingMoreThan, selectedLowToHigh} = this.state;
    const { dispatch } = this.props;

    if (store) {
      dispatch(ratingsActions.filterRatings(store.id, selectedRatingMoreThan.value, selectedLowToHigh.value));
    }

  }

  render() {
      const { stores } = this.props;
      const { ratings } = this.props;

      return (
        <div className="col-md-12 ratingPage">
          <div className="row filter-section" id="filter-section">
            <div className="col-md-3">
              {(stores && stores.length) ? (
                <Dropdown value={this.state.store} optionLabel="name" options={stores} onChange={this.onStoreChange} placeholder="Select a Store"/>
              ) : ''}
            </div>
            <div className="col-md-3">
              <Dropdown value={this.state.selectedRatingMoreThan} optionLabel="label" options={this.state.ratingsMoreThan} onChange={this.onRatingsMoreThanChange} placeholder="Rating More Than"/>
            </div>
            <div className="col-md-3">
              <Dropdown value={this.state.selectedLowToHigh} optionLabel="label" options={this.state.lowToHigh} onChange={this.onLowToHighChange} placeholder="Sorting"/>
            </div>
            <div className="col-md-3">
              {this.state.store ? (
                  <button className="btn btn-primary" id="filterButton" onClick={this.handleRatingSubmit}>Filter</button>
              ) : (
                  <button className="btn btn-primary" id="filterButton" onClick={this.handleRatingSubmit} disabled>Filter</button>
              )}
            </div>
          </div>
          <div>
            {ratings ? (
              <div>
                <div className="row">
                  <div className="col-md-12 text-center">
                    <p className="averageRating">Average Rate</p>
                    <p className="averageRating">{ratings.avgStoreRate} / 5</p>
                    <div className="mobile-visibility-only">
                      <StarRatings
                        rating={ratings.avgStoreRate}
                        starRatedColor="red"
                        starDimension={"40px"}
                        starSpacing="5px"
                        numberOfStars={5}
                        name='rating'
                      />
                    </div>
                    <div className="dekstop-visibility-only">
                      <StarRatings
                        rating={ratings.avgStoreRate}
                        starRatedColor="red"
                        starDimension={"90px"}
                        starSpacing="5px"
                        numberOfStars={5}
                        name='rating'
                      />
                    </div>
                  </div>
                </div>
                {'rateDetails' in ratings &&
                  <div className="row ratingBoxes">
                    {ratings['rateDetails'].map((rate, key) => {
                        return (
                          <div className="ratingBox col-sm-6 col-md-6 col-lg-3">
                            <div className={"box-rating"}>
                              <div className="row">
                                <div className="col-md-4">
                                  <img src="/assets/images/avatar.png" alt="Client"/>
                                </div>
                                <div className="col-md-8">
                                  <div className="clientName">{rate.userName}</div>
                                  <div className="clientRatingLabel">Speed Rate</div>
                                  <div className="clientSpeedRate">
                                      <StarRatings
                                        rating={rate.speedRate}
                                        starRatedColor="red"
                                        starDimension={"18px"}
                                        starSpacing="0px"
                                        numberOfStars={5}
                                        name='rating'
                                      />
                                  </div>
                                  <div className="clientRatingLabel">Quality Rate</div>
                                  <div className="clientQualityRate">
                                      <StarRatings
                                        rating={rate.qualityRate}
                                        starRatedColor="red"
                                        starDimension={"18px"}
                                        starSpacing="0px"
                                        numberOfStars={5}
                                        name='rating'
                                      />
                                  </div>
                                  <div className="clientDate">{this.changeDateFormat(rate.date)}</div>
                                  <div className="clientComment">{rate.comments ? "\""+rate.comments+"\"" : ""}</div>
                                </div>
                              </div>
                            </div>
                          </div>
                        )
                      }
                    )}
                  </div>
                }
              </div>
            )  : (
              <div className="row">
                <div className="col-md-12">
                  <p className="noResultsMsg">No results found!</p>
                </div>
              </div>
            )}
          </div>
        </div>
      );
  }
}

function mapStateToProps(state) {
    const { authentication, stores, ratings } = state;
    return {
        user: authentication, stores, ratings
    };
}

const connectedRatingPage = connect(mapStateToProps)(RatingPage);
export { connectedRatingPage as RatingPage };
