import React from 'react';
import { Link } from 'react-router-dom';
import { connect } from 'react-redux';

import { userActions } from '../../actions';


class ForgotPasswordPage extends React.Component {

    constructor(props) {
        super(props);

        // reset login status
        this.props.dispatch(userActions.logout());

        this.state = {
            username: '',
            error: false,
            success: false,
            submitted: false,
        };

        this.handleChange = this.handleChange.bind(this);
        this.handleSubmit = this.handleSubmit.bind(this);

    }

    handleChange(e) {
        const { name, value } = e.target;
        this.setState({ [name]: value });
    }

    handleSubmit(e) {
        e.preventDefault();
        this.setState({ error: false });
        this.setState({ submitted: true });
        const { username } = this.state;
        const { dispatch } = this.props;
        if (username ) {
          try {
            dispatch(userActions.checkifuserexists(username));
            setTimeout(function() {
              if (!this.props.user){
                this.setState({ error: true });
              }else{
                this.setState({ success: true });
              }
            }.bind(this), 1000);
          }
          catch(err) {

          }
        }
    }

    render() {
        const { user } = this.props;
        let { username, submitted, error, success } = this.state;

        return (
          <div  className="container" id="login-page">
            <div className="flex-block">
              <div className="col-md-6 login-flex-block">
                <div className="login-box">
                    <img src="/assets/images/logo.svg" alt="DeliveryMan" id="login-logo"/>
                    <p className="login-description">Forgot password</p>
                    <form name="form" onSubmit={this.handleSubmit}>
                        <div className={'form-group' + (submitted && (!username || error) ? ' has-error' : '')}>
                            <input type="text" className="form-control" name="username" placeholder="Username" value={username} onChange={this.handleChange} />
                            {submitted && !username &&
                                <div className="help-block">Username is required</div>
                            }
                        </div>
                        <div className={'form-group' + (submitted && success ? ' has-success' : '')}>
                            {submitted && error && !success &&
                              <div className="help-block"><p>Email does not exists</p></div>
                            }
                            {submitted && success && !error &&
                              <div className="help-block"><p>We have just sent you an email to reset your password.</p></div>
                            }
                            <button className="btn btn-primary btn-block">Get New Password</button>
                        </div>
                    </form>
                    <div className="paddingtop70">
                      <Link to="/" className="btn btn-primary">Take me Back</Link>
                    </div>
                </div>
              </div>
              <div className="col-md-6 login-flex-block" id="login-motor"></div>
              <div className="col-md-12 bottom-circles"></div>
            </div>
          </div>
        );
    }
}

function mapStateToProps(state) {
    const { user } = state.authentication;
    return {
        user
    };
}

const connectedLoginPage = connect(mapStateToProps)(ForgotPasswordPage);
export { connectedLoginPage as ForgotPasswordPage };
