import auth0 from 'auth0-js';

import { deleteCookie } from '../utils/cookies';
import { userConstants } from '../constants';



export const userService = {
    login,
    logout,
    socialMediaLogin,
    checkifuserexists
    // update
};

function login(username, password) {
  return dispatch => {
    let webAuth = new auth0.WebAuth({
      domain: 'deliveryman.eu.auth0.com',
      clientID: 'QksPgi5fGG5heFYTszvlGdTd3bxG99uB',
      redirectUri: `${window.location.protocol}//${window.location.host}/callback`,
      audience: 'deliveryman-auth.azurewebsites.net', //https://deliveryman.eu.auth0.com/userinfo',
      responseType: 'token id_token',
      scope: 'openid profile email'
    });

    webAuth.login({
        realm: 'Username-Password-Authentication',
        username: username,
        password: password
      }, function(error) {
            if (error) {
              dispatch(failure(error));
            }
          });
  };
  function failure(error) { return { type: userConstants.LOGIN_FAILURE, error } }

}

function socialMediaLogin(media) {

  let webAuth = new auth0.WebAuth({
    domain: 'deliveryman.eu.auth0.com',
    clientID: 'QksPgi5fGG5heFYTszvlGdTd3bxG99uB',
    redirectUri: `${window.location.protocol}//${window.location.host}/callback`,
    audience: 'deliveryman-auth.azurewebsites.net',
    responseType: 'token id_token',
    scope: 'openid profile email'
  });

  webAuth.authorize({
      connection: media,
    }, function(error, response) {
      if (error) {
        // console.log("error in service");
        return
      }else{
        // console.log(response);
      }
  });
}

function logout() {
    // remove user from cookies to log user out
  deleteCookie('token');
  localStorage.removeItem('user');
  localStorage.removeItem('stores');
  localStorage.removeItem('deliveryman-dateRange');
  localStorage.removeItem('deliveryman-store');
  localStorage.removeItem('deliveryman-rating-more-than');
  localStorage.removeItem('deliveryman-low-to-high');
  localStorage.removeItem('dm-api-token');
}


function checkifuserexists(username) {

}
