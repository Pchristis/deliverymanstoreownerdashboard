import auth0 from 'auth0-js';
import jwt_decode from 'jwt-decode';
import { setCookie,getCookie } from '../../utils/cookies';

export default class Auth {
  auth0 = new auth0.WebAuth({
    domain: 'deliveryman.eu.auth0.com',
    clientID: 'QksPgi5fGG5heFYTszvlGdTd3bxG99uB',
    redirectUri: `${window.location.protocol}//${window.location.host}/callback`,
    audience: 'deliveryman-auth.azurewebsites.net', //https://deliveryman.eu.auth0.com/userinfo',
    responseType: 'token id_token',
    scope: 'openid profile email'
  });

  constructor() {
    this.handleAuthentication = this.handleAuthentication.bind(this);
  }

  handleAuthentication(props) {
    this.auth0.parseHash((err, authResult) => {

      if (authResult && authResult.accessToken && authResult.idToken) {
        const stores = jwt_decode(authResult.accessToken);

        if (stores["https://www.deliveryman.com.cy/storeAccess"]){
          // save stores to props
          localStorage.setItem('stores', JSON.stringify(stores["https://www.deliveryman.com.cy/storeAccess"]));

          if (getCookie('rememberme')){
            setCookie('token', authResult.accessToken, 250);
          }else{
            setCookie('token', authResult.accessToken, 24);
          }
    			const userInfo = jwt_decode(authResult.idToken);
          localStorage.setItem('user', JSON.stringify(userInfo));
          props.history.push('/overview');

        }else{
           props.history.push('/');
        }

     }

     else if (err) {
       console.log("Error in Auth",err);
         props.history.push('/');
    }
   });
 }

}
