import React from 'react';
import { Switch, Router, Route } from 'react-router-dom';
import { connect } from 'react-redux';
 
import { history } from '../helpers';

/** Layouts **/
import LoginLayoutRoute from "../Layouts/LoginLayout";
import DashboardLayoutRoute from "../Layouts/Dashboard/DashboardLayout";

/** Pages **/
import { OverviewPage } from '../Pages/OverviewPage';
import { AnalysisPage } from '../Pages/AnalysisPage';
import { MenuPage } from '../Pages/MenuPage';
import { RatingPage } from '../Pages/RatingPage';
import { RestaurantControlPage } from '../Pages/RestaurantControlPage';
import { InvoicesPage } from '../Pages/InvoicesPage';
import { LoginPage } from '../Pages/LoginPage';
import { ForgotPasswordPage } from '../Pages/ForgotPasswordPage';
import { SettingsPage } from '../Pages/SettingsPage';
import NoMatchPage from '../Pages/NoMatchPage';


import Auth from "./Auth/Auth";

const authWeb = new Auth();

const handleAuthentication = (nextState, replace) => {
  if (/access_token|id_token|error/.test(nextState.location.hash)) {
    authWeb.handleAuthentication(nextState);
  }
};

class App extends React.Component {
    constructor(props) {
        super(props);

    }


    render() {
        return (
          <div className="wrapper">
            <Router history={history}>
                <div>
                    <Switch>
                      <Route path="/callback" name="Auth Callback" render={(props) => {
                        handleAuthentication(props);
                        return <OverviewPage {...props} />
                      }}/>

                      <LoginLayoutRoute path="/login" component={LoginPage} />
                      <LoginLayoutRoute path="/forgotpassword" component={ForgotPasswordPage} />
                      <DashboardLayoutRoute exact path="/" component={OverviewPage} />
                      <DashboardLayoutRoute exact path="/overview" component={OverviewPage} />
                      <DashboardLayoutRoute exact path="/analysis" component={AnalysisPage} />
                      <DashboardLayoutRoute exact path="/menu" component={MenuPage} />
                      <DashboardLayoutRoute exact path="/ratings" component={RatingPage} />
                      <DashboardLayoutRoute exact path="/storecontrol" component={RestaurantControlPage} />
                      <DashboardLayoutRoute exact path="/invoices" component={InvoicesPage} />
                      <DashboardLayoutRoute exact path="/settings" component={SettingsPage} />
                      <LoginLayoutRoute component={NoMatchPage} />
                    </Switch>
                </div>
            </Router>
          </div>
        );
    }
}

function mapStateToProps(state) {
    const { userinfo } = state;
    const { user } = userinfo;
    return {
      user: state.authentication, userinfo
    };
}

const connectedApp = connect(mapStateToProps)(App);
export { connectedApp as App };
