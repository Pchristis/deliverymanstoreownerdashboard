import { invoicesConstants } from '../constants';


export function invoices(state = [], action) {
  switch (action.type) {
    case invoicesConstants.INVOICES_SUCCESS:
      return action.invoices;
    default:
      return state
  }
}
