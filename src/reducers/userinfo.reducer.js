import { userConstants } from '../constants';

export function userinfo(state = {}, action) {

  switch (action.type) {
    case userConstants.UPDATE_USER_INFO:
      return action.user
    default:
      return state
  }
}
