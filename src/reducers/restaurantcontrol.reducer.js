import { restaurantcontrolConstants } from '../constants';


export function restaurantcontrol(state = [], action) {
  switch (action.type) {
    case restaurantcontrolConstants.RC_GET_DATA_SUCCESS:
      return action.restaurantcontrol;
    default:
      return state
  }
}
