import { overviewConstants } from '../constants';


export function overview(state = [], action) {
  switch (action.type) {
    case overviewConstants.OVERVIEW_SUCCESS:
      return action.overview;
    default:
      return state
  }
}
