import { menuCategoriesConstants } from '../constants';


export function menuCategories(state = [], action) {
  switch (action.type) {
    case menuCategoriesConstants.CATEGORIES_SUCCESS:
      return action.categories;
    default:
      return state
  }
}
