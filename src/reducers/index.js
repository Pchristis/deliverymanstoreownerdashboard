import { combineReducers } from 'redux';

import { authentication } from './authentication.reducer';
import { userinfo } from './userinfo.reducer';
import { stores } from './stores.reducer';
import { categories } from './categories.reducer';
import { overview } from './overview.reducer';
import { analysis } from './analysis.reducer';
import { orderanalysis } from './orderanalysis.reducer';
import { restaurantcontrol } from './restaurantcontrol.reducer';
import { menu } from './menu.reducer';
import { ratings } from './ratings.reducer';
import { invoices } from './invoices.reducer';
import {menuCategories} from './menuCategories.reducer';
import { reducer as formReducer } from 'redux-form';

const rootReducer = combineReducers({
  authentication,
  userinfo,
  stores,
  categories,
  overview,
  analysis,
  orderanalysis,
  restaurantcontrol,
  menu,
  ratings,
  invoices,
  menuCategories,
  form: formReducer
});

export default rootReducer;
