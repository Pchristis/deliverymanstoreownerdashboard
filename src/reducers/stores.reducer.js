import { storesConstants } from '../constants';

let storeslist = JSON.parse(localStorage.getItem('stores'));
const initialState = storeslist ? storeslist : {};

export function stores(state = initialState, action) {
  switch (action.type) {
    case storesConstants.STORES_REQUEST:
      return JSON.parse(localStorage.getItem('stores'))
    default:
      return state
  }
}
