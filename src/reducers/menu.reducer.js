import { menuConstants } from '../constants';


export function menu(state = [], action) {
  switch (action.type) {
    case menuConstants.MENU_SUCCESS:
      return action.menu;
    default:
      return state
  }
}
