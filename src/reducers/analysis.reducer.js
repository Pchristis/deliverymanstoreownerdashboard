import { analysisConstants } from '../constants';


export function analysis(state = [], action) {
  switch (action.type) {
    case analysisConstants.ANALYSIS_SUCCESS:
      return action.analysis;
    default:
      return state
  }
}
