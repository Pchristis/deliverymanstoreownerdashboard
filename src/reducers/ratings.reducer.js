import { ratingsConstants } from '../constants';


export function ratings(state = [], action) {
  switch (action.type) {
    case ratingsConstants.RATINGS_SUCCESS:
      return action.ratings;
    default:
      return state
  }
}
