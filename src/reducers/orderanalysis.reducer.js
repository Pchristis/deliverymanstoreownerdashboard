import { orderanalysisConstants } from '../constants';


export function orderanalysis(state = [], action) {
  switch (action.type) {
    case orderanalysisConstants.ORDER_ANALYSIS_SUCCESS:
      return action.orderanalysis;
    default:
      return state
  }
}
