import { userConstants } from '../constants';

let user = JSON.parse(localStorage.getItem('user'));
const initialState = user ? { loggedIn: true, user } : {};

export function authentication(state = initialState, action) {
  switch (action.type) {
    case userConstants.LOGIN_REQUEST:
      return {
        loggingIn: false,
        user: action.user
      };
    case userConstants.LOGIN_SUCCESS:
      return {
        loggedIn: true,
        user: action.user
      };
    case userConstants.LOGIN_FAILURE:
      return {
        loggingIn: false,
        user: action.error
      };
    case userConstants.LOGOUT:
      return {};
    case userConstants.IS_LOGGED_IN:
      return {
        loggedIn: true,
        user: JSON.parse(localStorage.getItem('user'))
      };
    case userConstants.FORGOT_REQUEST:
      return {
        loggingIn: false,
        user: action.user
      };
    default:
      return state
  }
}
