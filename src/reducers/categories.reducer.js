import { storesConstants } from '../constants';


export function categories(state = [], action) {
  switch (action.type) {
    case storesConstants.FILTER_CATEGORIES:
      return action.categories;
    default:
      return state
  }
}
